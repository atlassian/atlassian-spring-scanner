package com.atlassian.plugin.spring.scanner.test.otherplugin.impl;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.test.otherplugin.DynamicallyImportedServiceFromAnotherPlugin;

/**
 * This export is {@code ComponentImport}ed by the atlassian-spring-scanner-maven-test plugin
 */
@ExportAsService(DynamicallyImportedServiceFromAnotherPlugin.class)
@Component
@SuppressWarnings("unused")
public class DynamicallyImportedServiceFromAnotherPluginImpl implements DynamicallyImportedServiceFromAnotherPlugin {}
