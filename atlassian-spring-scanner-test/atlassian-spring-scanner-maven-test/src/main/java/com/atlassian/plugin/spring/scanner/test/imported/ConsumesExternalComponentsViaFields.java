package com.atlassian.plugin.spring.scanner.test.imported;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentComposite;

/**
 */
@Component
public class ConsumesExternalComponentsViaFields {
    @ClasspathComponent
    ExternalJarComponentComposite externalJarComponentComposite;
}
