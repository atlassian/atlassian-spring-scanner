package com.atlassian.plugin.spring.scanner.test.primary;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

/**
 * This is the primary implementation for the {@link ComponentInterfaceWithAPrimaryImplementation}. It is expected to be
 * autowired when {@link ComponentInterfaceWithAPrimaryImplementation} is requested
 */
@Primary
@Component
class PrimaryImplementation implements ComponentInterfaceWithAPrimaryImplementation {}
