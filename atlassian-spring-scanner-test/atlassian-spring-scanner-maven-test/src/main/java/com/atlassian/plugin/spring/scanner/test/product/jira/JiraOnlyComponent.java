package com.atlassian.plugin.spring.scanner.test.product.jira;

import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;

/**
 * A component that's only instantiated when running in JIRA
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@JiraComponent
public class JiraOnlyComponent {

    private final CommentService commentService;

    @Autowired
    public JiraOnlyComponent(@JiraImport CommentService commentService) {
        this.commentService = commentService;
    }
}
