package com.atlassian.plugin.spring.scanner.test.product.crowd;

import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.crowd.manager.mail.MailManager;
import com.atlassian.plugin.spring.scanner.annotation.component.CrowdComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.CrowdImport;

/**
 * A component that's only instantiated when running in Confluence
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@CrowdComponent
public class CrowdOnlyComponent {

    private final MailManager mailManager;

    @Autowired
    public CrowdOnlyComponent(@CrowdImport MailManager mailManager) {
        this.mailManager = mailManager;
    }
}
