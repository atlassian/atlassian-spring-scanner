package com.atlassian.plugin.spring.scanner.test.product.refapp;

import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.plugin.spring.scanner.annotation.component.RefappComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.RefappImport;
import com.atlassian.refapp.api.ConnectionProvider;

/**
 * A component that's only instantiated when running in Refapp
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@RefappComponent
public class RefappOnlyComponent {

    private final ConnectionProvider connectionProvider;

    @Autowired
    public RefappOnlyComponent(@RefappImport ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }
}
