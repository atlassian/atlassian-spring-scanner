package com.atlassian.plugin.spring.scanner.test.jsr;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.atlassian.plugin.spring.scanner.test.InternalComponent;
import com.atlassian.plugin.spring.scanner.test.InternalComponentTwo;

@SuppressWarnings("UnusedDeclaration")
@Singleton
public class JavaxSingletonComponent {

    private final InternalComponent internalComponent;

    /**
     * If we manage to instantiate this component, it must have been via this constructor, which means {@link Inject} is being processed correctly.
     */
    @Inject
    public JavaxSingletonComponent(InternalComponent internalComponent) {
        this.internalComponent = internalComponent;
    }

    public JavaxSingletonComponent(InternalComponentTwo otherNonDefaultConstructorThatNeedsDisambiguatingForSpring) {
        throw new IllegalStateException("The wrong constructor was called!");
    }
}
