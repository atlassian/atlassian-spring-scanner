package com.atlassian.plugin.spring.scanner.test.exported;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

@ExportAsService(ExposedAsAServiceComponentInterfaceThree.class)
@Component
public class ExposedAsAServiceComponentWithSpecifiedInterface
        implements ExposedAsAServiceComponentInterfaceThree, DisposableBean {
    @Override
    public void destroy() {}

    @Override
    public void doMoreStuff() {}
}
