package com.atlassian.plugin.spring.scanner.test.exported;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;

@ExportAsDevService
@Component
public class ExposedAsADevServiceComponent {}
