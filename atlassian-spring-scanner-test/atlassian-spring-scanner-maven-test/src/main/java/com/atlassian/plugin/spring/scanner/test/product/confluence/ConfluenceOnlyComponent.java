package com.atlassian.plugin.spring.scanner.test.product.confluence;

import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.confluence.api.service.content.ContentService;
import com.atlassian.plugin.spring.scanner.annotation.component.ConfluenceComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ConfluenceImport;

/**
 * A component that's only instantiated when running in Confluence
 */
@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@ConfluenceComponent
public class ConfluenceOnlyComponent {

    private final ContentService contentService;

    @Autowired
    public ConfluenceOnlyComponent(@ConfluenceImport ContentService contentService) {
        this.contentService = contentService;
    }
}
