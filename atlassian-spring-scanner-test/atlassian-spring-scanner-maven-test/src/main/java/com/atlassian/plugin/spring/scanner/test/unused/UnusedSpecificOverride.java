package com.atlassian.plugin.spring.scanner.test.unused;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.Profile;

@Profile("specific-override")
@Component
public class UnusedSpecificOverride {}
