package com.atlassian.plugin.spring.scanner.test.exported;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

@ExportAsService({ExposedAsAServiceComponentInterface.class, ExposedAsAServiceComponentInterfaceTwo.class})
@Component
public class ExposedAsAServiceComponentWithMultipleSpecifiedInterfaces
        implements ExposedAsAServiceComponentInterface, ExposedAsAServiceComponentInterfaceTwo, DisposableBean {
    @Override
    public void destroy() {}

    @Override
    public void doStuff() {}

    @Override
    public void doOtherStuff() {}
}
