package com.atlassian.plugin.spring.scanner.test.fields;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.test.NamedComponent;
import com.atlassian.plugin.spring.scanner.test.otherplugin.ServiceExportedFromAnotherPlugin;

@Component
public class ComponentWithFields {
    @ComponentImport
    @Autowired
    ServiceExportedFromAnotherPlugin externalService;

    @Autowired
    NamedComponent namedComponent;
}
