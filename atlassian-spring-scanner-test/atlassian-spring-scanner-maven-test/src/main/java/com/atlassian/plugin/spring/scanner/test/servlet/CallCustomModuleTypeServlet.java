package com.atlassian.plugin.spring.scanner.test.servlet;

import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.test.moduletype.BasicModuleDescriptor;

/**
 * Retrieves and calls {@link com.atlassian.plugin.spring.scanner.test.moduletype.BasicModuleDescriptor} so test can check it's working.
 */
@UnrestrictedAccess
public class CallCustomModuleTypeServlet extends HttpServlet {

    private final PluginAccessor pluginAccessor;

    @Inject
    public CallCustomModuleTypeServlet(@ComponentImport PluginAccessor pluginAccessor) {
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final List<BasicModuleDescriptor> enabledModuleDescriptorsByClass =
                pluginAccessor.getEnabledModuleDescriptorsByClass(BasicModuleDescriptor.class);

        if (enabledModuleDescriptorsByClass.size() != 1) {
            response.setStatus(500);
            throw new IllegalStateException("Test failed: expected exactly one BasicModuleDescriptor to be present "
                    + "but got: "
                    + enabledModuleDescriptorsByClass
                    + ". This probably means the @ModuleType annotation hasn't been processed correctly.");
        }

        response.setStatus(200);
        response.setContentType("text/plain");

        // Should call BasicModuleDescriptor.getModule(), which returns a string for the test to validate
        response.getWriter().write(enabledModuleDescriptorsByClass.get(0).getModule());

        response.flushBuffer();
    }
}
