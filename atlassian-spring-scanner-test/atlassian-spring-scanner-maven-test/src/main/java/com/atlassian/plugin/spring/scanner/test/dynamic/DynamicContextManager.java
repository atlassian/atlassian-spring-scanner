package com.atlassian.plugin.spring.scanner.test.dynamic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;
import org.osgi.framework.BundleContext;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.dynamic.contexts.DynamicContext;

/**
 * Allows other components to come into and out of existence dynamically.
 */
@SuppressWarnings({"SpringJavaAutowiringInspection", "FieldCanBeLocal", "UnusedDeclaration"})
@Component
public class DynamicContextManager {

    private final ApplicationContext parentContext;
    private final DynamicContext.Installer dynamicContextInstaller;

    private ConfigurableApplicationContext internalContext;

    @Autowired
    public DynamicContextManager(
            @ComponentImport final EventPublisher eventPublisher,
            @ComponentImport final PluginAccessor pluginAccessor,
            final ApplicationContext parentContext,
            final BundleContext bundleContext) {
        this.parentContext = parentContext;

        final String pluginKey = "com.atlassian.plugin.atlassian-spring-scanner-maven-test";
        final Plugin plugin = pluginAccessor.getPlugin(pluginKey);
        if (plugin == null) {
            throw new IllegalStateException("Plugin with key '" + pluginKey + "' was not found");
        }

        dynamicContextInstaller = DynamicContext.installer(eventPublisher, bundleContext, plugin.getKey());
    }

    public Result bootstrapTheRestOfTheApplication() {
        final long then = System.currentTimeMillis();
        if (internalContext == null) {
            this.internalContext = dynamicContextInstaller.useContext(
                    new String[] {"/dynamicProfile/dynamic-context-spring-scanner.xml"}, parentContext);

            return new Result(true, then);
        } else {
            return new Result(false, then);
        }
    }

    public Result shutdownNewContext() {
        final long then = System.currentTimeMillis();
        if (internalContext != null) {
            dynamicContextInstaller.closeAndUseContext(internalContext, internalContext.getParent());
            internalContext = null;
            return new Result(true, then);
        }
        return new Result(false, then);
    }

    public ConfigurableApplicationContext getInternalContext() {
        return internalContext;
    }

    public static class Result {

        private final boolean tookPlace;
        private final long timeTaken;

        Result(final boolean tookPlace, final long then) {
            this.tookPlace = tookPlace;
            this.timeTaken = System.currentTimeMillis() - then;
        }

        public boolean tookPlace() {
            return tookPlace;
        }

        public long getTimeTaken() {
            return timeTaken;
        }
    }
}
