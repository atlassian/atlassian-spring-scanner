package com.atlassian.plugin.spring.scanner.test.moduletype;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atlassian.plugin.hostcontainer.HostContainer;
import com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory;
import com.atlassian.plugin.osgi.external.SingleModuleDescriptorFactory;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;

@ModuleType(ListableModuleDescriptorFactory.class)
@Component
public class BasicModuleTypeFactory extends SingleModuleDescriptorFactory<BasicModuleDescriptor> {

    @Autowired
    public BasicModuleTypeFactory(final HostContainer hostContainer) {
        super(hostContainer, "basic", BasicModuleDescriptor.class);
    }
}
