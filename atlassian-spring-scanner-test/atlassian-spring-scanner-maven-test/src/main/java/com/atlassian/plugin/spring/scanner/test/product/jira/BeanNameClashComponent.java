package com.atlassian.plugin.spring.scanner.test.product.jira;

import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.sal.api.ApplicationProperties;

import static java.util.Objects.requireNonNull;

/**
 * A component that tests whether we can import two services that have the same simple class name.
 *
 * This didn't work in 2.2.0, because of SCANNER-2168. The workaround was to give one of these services a custom bean
 * name.
 */
@JiraComponent
@SuppressWarnings("unused")
public class BeanNameClashComponent {

    @Autowired
    public BeanNameClashComponent(
            @JiraImport final ApplicationProperties salApplicationProperties,
            @JiraImport final com.atlassian.jira.config.properties.ApplicationProperties jiraApplicationProperties) {
        requireNonNull(jiraApplicationProperties);
        requireNonNull(salApplicationProperties);
    }
}
