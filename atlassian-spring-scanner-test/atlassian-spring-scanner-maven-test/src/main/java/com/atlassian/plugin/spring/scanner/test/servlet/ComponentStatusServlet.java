package com.atlassian.plugin.spring.scanner.test.servlet;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.annotations.security.UnrestrictedAccess;
import com.atlassian.plugin.spring.scanner.test.registry.BeanLister;

/**
 * Returns status of live components, so test can assert against them.
 */
@UnrestrictedAccess
public class ComponentStatusServlet extends HttpServlet {
    private final BeanLister beanLister;

    @Inject
    public ComponentStatusServlet(BeanLister beanLister) {
        this.beanLister = beanLister;
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.setContentType("text/plain");

        boolean components = (request.getParameter("components") != null);
        boolean services = (request.getParameter("services") != null);
        if (!components && !services) {
            components = true;
            services = true;
        }

        if (components) {
            for (final String name : beanLister.listBeans()) {
                response.getWriter().println(name);
            }
        }

        if (services) {
            for (final String name : beanLister.listServices()) {
                response.getWriter().println(name);
            }
        }

        response.flushBuffer();
    }
}
