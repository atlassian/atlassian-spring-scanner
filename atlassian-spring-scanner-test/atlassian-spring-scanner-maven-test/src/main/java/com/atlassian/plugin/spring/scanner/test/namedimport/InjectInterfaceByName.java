package com.atlassian.plugin.spring.scanner.test.namedimport;

import javax.inject.Named;

import com.atlassian.plugin.PluginAccessor;

/**
 * Tests that putting {@link Named} on a constructor argument doesn't create an invalid component index entry
 * that could cause a desired bean definition to be overwritten, see
 * <a href="https://ecosystem.atlassian.net/browse/SCANNER-2179">SCANNER-2179</a>.
 *
 * @since 2.1.17
 */
@SuppressWarnings("unused")
public class InjectInterfaceByName {

    public InjectInterfaceByName(@Named("pluginAccessor") final PluginAccessor pluginAccessor) {
        // Nothing to do
    }
}
