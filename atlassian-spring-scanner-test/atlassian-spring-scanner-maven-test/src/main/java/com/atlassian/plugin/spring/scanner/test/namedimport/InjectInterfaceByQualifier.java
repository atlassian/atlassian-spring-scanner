package com.atlassian.plugin.spring.scanner.test.namedimport;

import org.springframework.beans.factory.annotation.Qualifier;

import com.atlassian.plugin.PluginAccessor;

/**
 * Tests that putting Spring's {@link Qualifier} on a constructor argument doesn't create an unwanted bean definition
 * that potentially overwrites a desired bean definition. This is to prevent regressions when fixing
 * <a href="https://ecosystem.atlassian.net/browse/SCANNER-2179">SCANNER-2179</a>.
 *
 * @since 2.1.17
 */
@SuppressWarnings("unused")
public class InjectInterfaceByQualifier {

    public InjectInterfaceByQualifier(@Qualifier("pluginAccessor") final PluginAccessor pluginAccessor) {
        // Nothing to do
    }
}
