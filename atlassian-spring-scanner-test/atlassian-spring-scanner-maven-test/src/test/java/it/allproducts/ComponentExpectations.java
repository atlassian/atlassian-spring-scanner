package it.allproducts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import org.springframework.stereotype.Component;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static java.lang.String.join;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.startsWith;

/**
 * Abstracts expected components across {@link TestPackaging} and {@link it.perproduct.AbstractComponentsInProductTest}
 * subclasses - ensures our packaging tests match our runtime tests, etc.
 *
 * The expectations in this class reflect the {@link Component}, {@link ComponentImport}, {@link ExportAsService} etc.
 * in the test plugin.
 */
@ParametersAreNonnullByDefault
public class ComponentExpectations {

    public static final ComponentExpectations COMMON =
            new ComponentExpectations(emptyList(), emptyList(), emptyList(), emptyList());

    public static final ComponentExpectations REFAPP = new ComponentExpectations(
            // RefappComponent
            singleton(component("com.atlassian.plugin.spring.scanner.test.product.refapp.RefappOnlyComponent")),
            // RefappImport
            singleton(componentImport("com.atlassian.refapp.api.ConnectionProvider")),
            // Extra components that only appear in Refapp
            asList(
                    autoAddedComponent("org.springframework.context.event.internalEventListenerFactory"),
                    autoAddedComponent("org.springframework.context.event.internalEventListenerProcessor")),
            emptyList());

    public static final ComponentExpectations CONFLUENCE = new ComponentExpectations(
            // ConfluenceComponent
            singleton(component("com.atlassian.plugin.spring.scanner.test.product.confluence.ConfluenceOnlyComponent")),
            // ConfluenceImport
            singleton(componentImport("com.atlassian.confluence.api.service.content.ContentService")),
            // Extra components that only appear in Confluence
            asList(
                    autoAddedComponent("org.springframework.context.event.internalEventListenerFactory"),
                    autoAddedComponent("org.springframework.context.event.internalEventListenerProcessor")),
            emptyList());

    public static final ComponentExpectations CROWD = new ComponentExpectations(
            // CrowdComponent
            singleton(component("com.atlassian.plugin.spring.scanner.test.product.crowd.CrowdOnlyComponent")),
            // CrowdImport
            singleton(componentImport("com.atlassian.crowd.manager.mail.MailManager")),
            asList(
                    autoAddedComponent("org.springframework.context.event.internalEventListenerFactory"),
                    autoAddedComponent("org.springframework.context.event.internalEventListenerProcessor")),
            emptyList());

    public static final ComponentExpectations JIRA = new ComponentExpectations(
            // JiraComponent
            asList(
                    component("com.atlassian.plugin.spring.scanner.test.product.jira.BeanNameClashComponent"),
                    component("com.atlassian.plugin.spring.scanner.test.product.jira.JiraOnlyComponent")),
            // JiraImport
            asList(
                    componentImport("com.atlassian.jira.bc.issue.comment.CommentService"),
                    componentImport("com.atlassian.jira.config.properties.ApplicationProperties"),
                    componentImport("com.atlassian.sal.api.ApplicationProperties")),
            asList(
                    autoAddedComponent("org.springframework.context.event.internalEventListenerFactory"),
                    autoAddedComponent("org.springframework.context.event.internalEventListenerProcessor")),
            emptyList());

    public static final ComponentExpectations BAMBOO = new ComponentExpectations(
            // BambooComponent
            singleton(component("com.atlassian.plugin.spring.scanner.test.product.bamboo.BambooOnlyComponent")),
            // BambooImport
            singleton(componentImport("com.atlassian.bamboo.build.BuildExecutionManager")),
            asList(
                    autoAddedComponent("org.springframework.context.event.internalEventListenerFactory"),
                    autoAddedComponent("org.springframework.context.event.internalEventListenerProcessor")),
            emptyList());

    public static final ComponentExpectations BITBUCKET = new ComponentExpectations(
            // BitbucketComponent
            singleton(component("com.atlassian.plugin.spring.scanner.test.product.bitbucket.BitbucketOnlyComponent")),
            // BitbucketImport
            singleton(componentImport("com.atlassian.bitbucket.repository.RepositoryService")),
            asList(
                    autoAddedComponent("org.springframework.context.event.internalEventListenerFactory"),
                    autoAddedComponent("org.springframework.context.event.internalEventListenerProcessor")),
            // Bitbucket adds the following LifecycleAware to certain plugins, see BeanEventListenerScanner in BBS
            singleton(serviceExport(
                    "com.atlassian.bitbucket.internal.plugin.extender.BeanEventListenerScanner.EventListenerLifecycle",
                    singletonList("com.atlassian.sal.api.lifecycle.LifecycleAware"))));

    public static final ComponentExpectations FECRU = new ComponentExpectations(
            // FecruComponent
            singleton(component("com.atlassian.plugin.spring.scanner.test.product.fecru.FecruOnlyComponent")),
            // FecruImport
            singleton(componentImport("com.atlassian.fisheye.spi.services.RepositoryService")),
            emptyList(),
            emptyList());

    private final List<ExpectedComponent> productSpecificScannerCreatedComponents;
    private final List<ExpectedComponentImport> productSpecificComponentImports;
    private final List<ExpectedAutoAddedComponent> productSpecificAutoAddedComponents;
    private final List<ExpectedServiceExport> productSpecificAutoExportedServices;

    private ComponentExpectations(
            final Collection<ExpectedComponent> productSpecificComponents,
            final Collection<ExpectedComponentImport> productSpecificComponentImports,
            final Collection<ExpectedAutoAddedComponent> productSpecificAutoAddedComponents,
            final Collection<ExpectedServiceExport> productSpecificAutoExportedServices) {
        this.productSpecificScannerCreatedComponents = new ArrayList<>(productSpecificComponents);
        this.productSpecificComponentImports = new ArrayList<>(productSpecificComponentImports);
        this.productSpecificAutoAddedComponents = new ArrayList<>(productSpecificAutoAddedComponents);
        this.productSpecificAutoExportedServices = new ArrayList<>(productSpecificAutoExportedServices);
    }

    /**
     * Things in META-INF/plugin-components/components
     */
    public List<ExpectedComponent> getExpectedScannerCreatedComponents() {
        return asList(
                // Test plugin's components instantiated in different ways
                component("com.atlassian.plugin.spring.scanner.test.ConsumingInternalOnlyComponent"),
                component("com.atlassian.plugin.spring.scanner.test.imported.ConsumingMixedComponents"),
                component("com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponent"),
                component(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentWithSpecifiedInterface"),
                component(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentWithMultipleSpecifiedInterfaces"),
                component("com.atlassian.plugin.spring.scanner.test.exported.ExposedAsADevServiceComponent"),
                component(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalServiceViaField"),
                component(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalDevServiceViaField"),
                component("com.atlassian.plugin.spring.scanner.test.InternalComponent"),
                component("com.atlassian.plugin.spring.scanner.test.InternalComponentTwo"),
                component("com.atlassian.plugin.spring.scanner.test.NamedComponent", "namedComponent"),
                component(
                        "com.atlassian.plugin.spring.scanner.test.imported.NamedConsumingMixedComponents",
                        "namedMixed"),
                component(
                        "com.atlassian.plugin.spring.scanner.test.OuterClass$InnerClass$InnerComponent$EvenMoreInnerComponent"),
                component("com.atlassian.plugin.spring.scanner.test.OuterClass$InnerClass$InnerComponent"),
                component("com.atlassian.plugin.spring.scanner.test.OuterClass$OuterComponent"),
                component("com.atlassian.plugin.spring.scanner.test.primary.BaseImplementation"),
                component("com.atlassian.plugin.spring.scanner.test.primary.PrimaryImplementation"),
                component("com.atlassian.plugin.spring.scanner.test.primary.ConsumingPrimaryComponentByInterface"),
                component("com.atlassian.plugin.spring.scanner.test.fields.ComponentWithFields"),
                component("com.atlassian.plugin.spring.scanner.test.spring.ControllerComponent"),
                component("com.atlassian.plugin.spring.scanner.test.spring.RepositoryComponent"),
                component("com.atlassian.plugin.spring.scanner.test.spring.ServiceComponent"),
                component("com.atlassian.plugin.spring.scanner.test.jsr.JsrComponent"),
                component("com.atlassian.plugin.spring.scanner.test.jsr.JavaxSingletonComponent"),
                component("com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentOne"),
                component("com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentTwo"),
                component("com.atlassian.plugin.spring.scanner.external.component.ExternalJarComponentComposite"),
                component("com.atlassian.plugin.spring.scanner.external.notannotated.NotAnnotatedExternalJarClass"),
                component("com.atlassian.plugin.spring.scanner.external.component.TransitiveJarComponent1"),
                component("com.atlassian.plugin.spring.scanner.external.component.TransitiveJarComponent2"),
                component("com.atlassian.plugin.spring.scanner.external.component.TransitiveJarComponentComposite"),
                component("com.atlassian.plugin.spring.scanner.test.imported.ConsumesExternalComponentsViaConstructor"),
                component("com.atlassian.plugin.spring.scanner.test.imported.ConsumesExternalComponentsViaFields"),

                // Test plugin's components - test infrastructure
                component("com.atlassian.plugin.spring.scanner.test.dynamic.DynamicContextManager"),
                component("com.atlassian.plugin.spring.scanner.test.registry.BeanLister"),

                // Module type
                component("com.atlassian.plugin.spring.scanner.test.moduletype.BasicModuleTypeFactory"),
                // because we have a @ModuleType, we get an extra component in support of that
                component("com.atlassian.plugin.osgi.bridge.external.SpringHostContainer"));
    }

    /**
     * Common components instantiated by scanner programmatically: don't appear in META-INF/plugin-components
     */
    public List<ExpectedAutoAddedComponent> getExpectedAutoAddedComponents() {
        return asList(
                // Scanner's internal components auto-added to spring context
                autoAddedComponent(
                        "devModeBeanInitialisationLoggerBeanPostProcessor",
                        "com.atlassian.plugin.spring.scanner.runtime.impl.DevModeBeanInitialisationLoggerBeanPostProcessor"),
                autoAddedComponent(
                        "serviceExportBeanPostProcessor",
                        "com.atlassian.plugin.spring.scanner.runtime.impl.ServiceExporterBeanPostProcessor"),
                autoAddedComponent(
                        "componentImportBeanFactoryPostProcessor",
                        "com.atlassian.plugin.spring.scanner.runtime.impl.ComponentImportBeanFactoryPostProcessor"));
    }

    public List<ExpectedAutoAddedComponent> getExpectedAutoAddedComponentsSpring5() {
        return asList(
                // Spring internal components
                autoAddedComponent(
                        "org.springframework.context.annotation.internalAutowiredAnnotationProcessor",
                        "org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor"),
                autoAddedComponent(
                        "org.springframework.context.annotation.internalCommonAnnotationProcessor",
                        "org.springframework.context.annotation.CommonAnnotationBeanPostProcessor"),
                autoAddedComponent(
                        "org.springframework.context.annotation.internalConfigurationAnnotationProcessor",
                        "org.springframework.context.annotation.ConfigurationClassPostProcessor"));
    }

    public List<ExpectedAutoAddedComponent> getExpectedAutoAddedComponentsSpring() {
        return asList(
                // Spring internal components
                autoAddedComponent(
                        "org.springframework.context.annotation.internalAutowiredAnnotationProcessor",
                        "org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor"),
                autoAddedComponent(
                        "org.springframework.context.annotation.internalCommonAnnotationProcessor",
                        "org.springframework.context.annotation.CommonAnnotationBeanPostProcessor"),
                autoAddedComponent(
                        "org.springframework.context.annotation.internalConfigurationAnnotationProcessor",
                        "org.springframework.context.annotation.ConfigurationClassPostProcessor"),
                autoAddedComponent(
                        "org.springframework.context.annotation.internalRequiredAnnotationProcessor",
                        "org.springframework.beans.factory.annotation.RequiredAnnotationBeanPostProcessor"));
    }

    /**
     * Things in META-INF/plugin-components/imports
     */
    public List<ExpectedComponentImport> getExpectedScannerCreatedComponentImports() {
        return asList(
                componentImport("com.atlassian.event.api.EventPublisher"),
                componentImport("com.atlassian.plugin.PluginAccessor"),
                componentImport("com.atlassian.plugin.module.ModuleFactory"),
                componentImport(
                        "com.atlassian.plugin.spring.scanner.test.otherplugin.ServiceExportedFromAnotherPlugin"),
                // componentImport("ServiceTwoExportedFromAnotherPlugin"), is only in dynamic profile
                componentImport(
                        "com.atlassian.plugin.spring.scanner.test.otherplugin.ServiceTwoExportedFromAnotherPlugin"));
    }

    /**
     * Things in META-INF/plugin-components/exports
     */
    public List<ExpectedServiceExport> getExpectedScannerCreatedExports() {
        return asList(
                // Explicitly exported services
                serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponent",
                        singletonList(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterface")),
                serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentWithSpecifiedInterface",
                        singletonList(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterfaceThree"),
                        singletonList(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterfaceThree")),
                serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentWithMultipleSpecifiedInterfaces",
                        asList(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterface",
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterfaceTwo"),
                        asList(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterface",
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsAServiceComponentInterfaceTwo")),
                serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalServiceViaField",
                        singletonList(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalServiceViaField")),

                // Module type
                serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.moduletype.BasicModuleTypeFactory",
                        singletonList("com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory"),
                        singletonList("com.atlassian.plugin.osgi.external.ListableModuleDescriptorFactory")));
    }

    /**
     * Things in META-INF/plugin-components/dev-exports
     */
    public List<ExpectedServiceExport> getExpectedScannerCreatedDevExports() {
        return asList(
                serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsADevServiceComponent",
                        singletonList(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExposedAsADevServiceComponent")),
                serviceExport(
                        "com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalDevServiceViaField",
                        singletonList(
                                "com.atlassian.plugin.spring.scanner.test.exported.ExportExternalComponentsViaFields$ExternalDevServiceViaField")));
    }

    /**
     * Common stuff made by scanner programmatically: don't appear in META-INF/plugin-components
     */
    public List<ExpectedServiceExport> getExpectedAutoAddedExports() {
        return singletonList(
                // Gemini's common services
                serviceExport(
                        null,
                        asList(
                                "org.eclipse.gemini.blueprint.context.DelegatedExecutionOsgiBundleApplicationContext",
                                "org.eclipse.gemini.blueprint.context.ConfigurableOsgiBundleApplicationContext",
                                "org.springframework.context.ConfigurableApplicationContext",
                                "org.springframework.context.ApplicationContext",
                                "org.springframework.context.Lifecycle",
                                "java.io.Closeable",
                                "org.springframework.beans.factory.ListableBeanFactory",
                                "org.springframework.beans.factory.HierarchicalBeanFactory",
                                "org.springframework.context.MessageSource",
                                "org.springframework.context.ApplicationEventPublisher",
                                "org.springframework.core.io.support.ResourcePatternResolver",
                                "org.springframework.beans.factory.BeanFactory",
                                "org.springframework.core.io.ResourceLoader",
                                "java.lang.AutoCloseable",
                                "org.springframework.beans.factory.DisposableBean")));
    }

    /**
     * Things in META-INF/plugin-components/profile-dynamic/component
     */
    public List<ExpectedComponent> getExpectedScannerCreatedDynamicComponents() {
        return singletonList(component("com.atlassian.plugin.spring.scanner.test.dynamic.DynamicComponent"));
    }

    /**
     * Things in META-INF/plugin-components/profile-dynamic/imports
     */
    public List<ExpectedComponentImport> getExpectedScannerCreatedDynamicComponentImports() {
        return singletonList(componentImport(
                "com.atlassian.plugin.spring.scanner.test.otherplugin.DynamicallyImportedServiceFromAnotherPlugin"));
    }

    /**
     * Things in META-INF/plugin-components/component-{product}
     */
    public List<ExpectedComponent> getProductSpecificScannerCreatedComponents() {
        return productSpecificScannerCreatedComponents;
    }

    /**
     * Things in META-INF/plugin-components/imports-{product}
     */
    public List<ExpectedComponentImport> getProductSpecificScannerCreatedComponentImports() {
        return productSpecificComponentImports;
    }

    /**
     * Product-specific stuff made by product programmatically: don't appear in META-INF/component-{product}
     */
    public List<ExpectedAutoAddedComponent> getProductSpecificAutoAddedComponents() {
        return productSpecificAutoAddedComponents;
    }

    /**
     * Product-specific exports made by product programmatically.
     *
     * @return a non-null list
     */
    @Nonnull
    public final List<ExpectedServiceExport> getProductSpecificAutoExportedServices() {
        return productSpecificAutoExportedServices;
    }

    private static String toComponentName(String classNameWithPackage) {
        final String simpleClassName =
                classNameWithPackage.replaceAll(".*\\.", "").replace('$', '.');
        return simpleClassName.substring(0, 1).toLowerCase() + simpleClassName.substring(1);
    }

    private static ExpectedComponent component(String implClass) {
        return new ExpectedComponent(implClass, null);
    }

    private static ExpectedComponent component(String implClass, String specifiedName) {
        return new ExpectedComponent(implClass, specifiedName);
    }

    private static ExpectedComponentImport componentImport(String iface) {
        return new ExpectedComponentImport(iface);
    }

    private static ExpectedAutoAddedComponent autoAddedComponent(String name) {
        return new ExpectedAutoAddedComponent(name);
    }

    private static ExpectedAutoAddedComponent autoAddedComponent(String name, String implClass) {
        return new ExpectedAutoAddedComponent(name, implClass);
    }

    private static ExpectedServiceExport serviceExport(
            @Nullable final String implClass, final List<String> expectedExportedInterfaces) {
        return new ExpectedServiceExport(implClass, expectedExportedInterfaces, null);
    }

    private static ExpectedServiceExport serviceExport(
            String implClass, List<String> expectedExportedInterfaces, List<String> specifiedInterfaces) {
        return new ExpectedServiceExport(implClass, expectedExportedInterfaces, specifiedInterfaces);
    }

    public abstract static class AbstractExpectedComponent {
        /**
         * What do we expect to appear in META-INF/plugin-components index file lines?
         *
         * @see TestPackaging
         */
        public abstract Matcher<String> getIndexLineMatcher();

        /**
         * What do we expect to appear as a runtime component
         * as output by {@link com.atlassian.plugin.spring.scanner.test.servlet.ComponentStatusServlet}?
         *
         * @see it.perproduct.AbstractComponentsInProductTest
         */
        public abstract Matcher<String> getRuntimeComponentEntryMatcher();
    }

    public static class ExpectedComponent extends AbstractExpectedComponent {

        private final String implClass;
        private final String specifiedName;

        public ExpectedComponent(final String implClass, @Nullable final String specifiedName) {
            this.implClass = implClass;
            this.specifiedName = specifiedName;
        }

        @Override
        public Matcher<String> getIndexLineMatcher() {
            return equalTo(implClass + ((specifiedName == null) ? "" : "#" + specifiedName));
        }

        @Override
        public Matcher<String> getRuntimeComponentEntryMatcher() {
            return equalTo(((specifiedName != null) ? specifiedName : toComponentName(implClass)) + " = " + implClass);
        }
    }

    public static class ExpectedComponentImport extends AbstractExpectedComponent {

        private final String importedInterface;

        private ExpectedComponentImport(final String importedInterface) {
            this.importedInterface = importedInterface;
        }

        @Override
        public Matcher<String> getIndexLineMatcher() {
            return equalTo(importedInterface);
        }

        @Override
        public Matcher<String> getRuntimeComponentEntryMatcher() {
            return Matchers.anyOf(
                    new AnyImplClassComponentMatcher(toComponentName(importedInterface)),
                    startsWith(importedInterface));
        }
    }

    public static class ExpectedAutoAddedComponent extends AbstractExpectedComponent {

        private final String name;
        private final String implClass;

        private ExpectedAutoAddedComponent(String name) {
            this.name = name;
            this.implClass = null;
        }

        private ExpectedAutoAddedComponent(String name, String implClass) {
            this.name = name;
            this.implClass = implClass;
        }

        @Override
        public Matcher<String> getIndexLineMatcher() {
            // Created at runtime, doesn't appear in index files
            return null;
        }

        @Override
        public Matcher<String> getRuntimeComponentEntryMatcher() {
            if (implClass == null) {
                // Don't care much about actual imported impl class - some of them vary by product
                return new AnyImplClassComponentMatcher(name);
            }
            return equalTo(name + " = " + implClass);
        }
    }

    public static class ExpectedServiceExport extends AbstractExpectedComponent {

        private final String implClass;
        private final List<String> expectedExportedInterfaces;
        private final List<String> specifiedInterfaces;

        private ExpectedServiceExport(
                @Nullable final String implClass,
                final List<String> expectedExportedInterfaces,
                @Nullable final List<String> specifiedInterfaces) {
            this.implClass = implClass;
            this.expectedExportedInterfaces = expectedExportedInterfaces;
            this.specifiedInterfaces = specifiedInterfaces;
        }

        @Override
        public Matcher<String> getIndexLineMatcher() {
            return equalTo(implClass + ((specifiedInterfaces != null) ? ("#" + join(",", specifiedInterfaces)) : ""));
        }

        @Override
        public Matcher<String> getRuntimeComponentEntryMatcher() {
            return new InterfaceMatcherInAnyOrder(expectedExportedInterfaces);
        }
    }

    private static class InterfaceMatcherInAnyOrder extends TypeSafeMatcher<String> {

        private final List<String> interfaces;

        public InterfaceMatcherInAnyOrder(final List<String> interfaces) {
            this.interfaces = interfaces;
        }

        @Override
        protected boolean matchesSafely(final String item) {
            final Collection<Matcher<? super String>> expected =
                    interfaces.stream().map(Matchers::equalTo).collect(toList());
            final Iterable<String> actual = asList(item.split(","));
            final Matcher<Iterable<? extends String>> iterableMatcher = containsInAnyOrder(expected);
            return iterableMatcher.matches(actual);
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("exported interfaces: '" + interfaces + "'");
        }
    }

    /**
     * Match expected components by name only, coping with implementation classes that vary between products.
     */
    private static class AnyImplClassComponentMatcher extends TypeSafeMatcher<String> {

        private final String componentName;

        public AnyImplClassComponentMatcher(String componentName) {
            this.componentName = componentName;
        }

        @Override
        protected boolean matchesSafely(String item) {
            // Wildcards for cross-product variability in impl classes: e.g. matches both:
            //   eventPublisher = com.atlassian.event.internal.EventPublisherImpl
            //   eventPublisher = com.atlassian.confluence.event.TimingEventPublisher
            return item.startsWith(componentName + " = ");
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("component named: '" + componentName + "' (any impl)");
        }
    }
}
