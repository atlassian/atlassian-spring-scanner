package it.allproducts;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import org.hamcrest.Matcher;
import org.junit.Test;

import it.allproducts.ComponentExpectations.AbstractExpectedComponent;
import it.perproduct.AbstractComponentsInProductTest;

import com.atlassian.plugin.spring.scanner.test.InternalComponent;

import static it.allproducts.ComponentExpectations.BAMBOO;
import static it.allproducts.ComponentExpectations.BITBUCKET;
import static it.allproducts.ComponentExpectations.COMMON;
import static it.allproducts.ComponentExpectations.CONFLUENCE;
import static it.allproducts.ComponentExpectations.CROWD;
import static it.allproducts.ComponentExpectations.FECRU;
import static it.allproducts.ComponentExpectations.JIRA;
import static it.allproducts.ComponentExpectations.REFAPP;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.io.IOUtils.readLines;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertNotNull;

/**
 * Checks that the index files produced by the {@code atlassian-spring-scanner-maven-plugin} are what we expect, given
 * the annotations used in this plugin.
 * <p>
 * Note this test is only for the <b>build-time</b> part of Spring Scanner - it simply validates what happens during the
 * {@code packaging} phase. This test runs in the {@code integration-test} phase due to Maven classpath issues.
 * It doesn't assert anything about the plugin running in the product - it's actually just looking at the index files
 * located either in your {@code /target/classes} directory (if run from IDEA) or this module's jar file (when run via
 * Maven).
 * <p>
 * To get valid results when run from the IDE, you first need to re-generate the index files by running
 * {@code mvn package} for this module, e.g.
 * {@code mvn package -pl atlassian-spring-scanner-test/atlassian-spring-scanner-maven-test}.
 *
 * @see AbstractComponentsInProductTest subclasses for runtime tests
 */
public class TestPackaging {

    @Test
    public void testExpectedComponents() throws Exception {
        assertIndexContents("component", COMMON.getExpectedScannerCreatedComponents());
    }

    @Test
    public void testExpectedImports() throws Exception {
        assertIndexContents("imports", COMMON.getExpectedScannerCreatedComponentImports());
    }

    @Test
    public void testExpectedExports() throws Exception {
        assertIndexContents("exports", COMMON.getExpectedScannerCreatedExports());
    }

    @Test
    public void testExpectedDevExports() throws Exception {
        assertIndexContents("dev-exports", COMMON.getExpectedScannerCreatedDevExports());
    }

    @Test
    public void testExpectedDynamicComponents() throws Exception {
        assertIndexContents("profile-dynamic/component", COMMON.getExpectedScannerCreatedDynamicComponents());
    }

    @Test
    public void testExpectedDynamicImports() throws Exception {
        assertIndexContents("profile-dynamic/imports", COMMON.getExpectedScannerCreatedDynamicComponentImports());
    }

    @Test
    public void testExpectedPackageInfoComponents() throws Exception {
        // these two use package info level annotations
        assertIndexContents(
                "profile-unused/component",
                "com.atlassian.plugin.spring.scanner.test.unused.UnusedComponent1",
                "com.atlassian.plugin.spring.scanner.test.unused.UnusedComponent2");

        // this specific class has another profile override and hence does NOT take the package level ones
        // It cumulative inheritance, its override specifics
        assertIndexContents(
                "profile-specific-override/component",
                "com.atlassian.plugin.spring.scanner.test.unused.UnusedSpecificOverride");
    }

    @Test
    public void testBambooComponents() throws Exception {
        testProductSpecificComponents("bamboo", BAMBOO);
    }

    @Test
    public void testBitbucketComponents() throws Exception {
        testProductSpecificComponents("bitbucket", BITBUCKET);
    }

    @Test
    public void testConfluenceComponents() throws Exception {
        testProductSpecificComponents("confluence", CONFLUENCE);
    }

    @Test
    public void testFecruComponents() throws Exception {
        testProductSpecificComponents("fecru", FECRU);
    }

    @Test
    public void testJiraComponents() throws Exception {
        testProductSpecificComponents("jira", JIRA);
    }

    @Test
    public void testRefappComponents() throws Exception {
        testProductSpecificComponents("refapp", REFAPP);
    }

    @Test
    public void testCrowdComponents() throws Exception {
        testProductSpecificComponents("crowd", CROWD);
    }

    private static void testProductSpecificComponents(
            final String indexFileQualifier, final ComponentExpectations productSpecificExpectations)
            throws IOException {
        assertIndexContents(
                "component-" + indexFileQualifier,
                productSpecificExpectations.getProductSpecificScannerCreatedComponents());
        assertIndexContents(
                "imports-" + indexFileQualifier,
                productSpecificExpectations.getProductSpecificScannerCreatedComponentImports());
    }

    private static void assertIndexContents(
            final String indexFilename, final List<? extends AbstractExpectedComponent> expectedItems)
            throws IOException {
        final List<String> actual = readIndexFile(indexFilename);
        final Collection<Matcher<? super String>> lineMatchers = expectedItems.stream()
                .map(AbstractExpectedComponent::getIndexLineMatcher)
                .collect(toList());
        assertThat(actual, containsInAnyOrder(lineMatchers));
    }

    private static void assertIndexContents(final String indexFilename, final String... expected) throws IOException {
        final List<String> actual = readIndexFile(indexFilename);
        assertThat(actual, containsInAnyOrder(expected));
    }

    private static List<String> readIndexFile(final String indexFileName) throws IOException {
        final String indexFilePath = "META-INF/plugin-components/" + indexFileName;
        // Load the index file using the classloader of a class within the test plugin
        final ClassLoader classLoader = InternalComponent.class.getClassLoader();
        try (final InputStream inputStream = classLoader.getResourceAsStream(indexFilePath)) {
            assertNotNull("Unable to read expected index file '" + indexFilePath + "'", inputStream);
            return readLines(inputStream, UTF_8);
        }
    }
}
