package it.perproduct;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.apache.commons.io.IOUtils;

import static java.nio.charset.StandardCharsets.UTF_8;

public abstract class AbstractInProductTest {

    /**
     * If running from maven, the property is set by AMPS, from the amps-maven-plugin product config in this module.
     *
     * The AMPS config also overrides this to be the same for every product, so we don't have to have product-specific
     * defaults to run these tests in IDEA.
     */
    protected static final String BASEURL = System.getProperty("baseurl", "http://localhost:5990/product");

    /**
     * Reads the contents of the given URL as a list of Strings.
     *
     * @param url the URL to read, as UTF-8
     * @return the contents
     * @throws IOException if there's an I/O error
     */
    protected static List<String> readStringList(final String url) throws IOException {
        final URLConnection connection = new URL(url).openConnection();
        try (final InputStream in = connection.getInputStream()) {
            return IOUtils.readLines(in, UTF_8);
        }
    }

    /**
     * Reads the contents of the given URL as a String.
     *
     * @param url the URL to read, as UTF-8
     * @return the contents
     * @throws IOException if there's an I/O error
     */
    protected static String getUrl(final String url) throws IOException {
        return IOUtils.toString(new URL(url), UTF_8);
    }
}
