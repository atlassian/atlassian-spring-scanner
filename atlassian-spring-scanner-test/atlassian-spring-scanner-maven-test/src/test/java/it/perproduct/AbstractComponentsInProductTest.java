package it.perproduct;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import org.apache.maven.artifact.versioning.ComparableVersion;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Test;

import it.allproducts.ComponentExpectations;
import it.allproducts.ComponentExpectations.AbstractExpectedComponent;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.test.servlet.ComponentStatusServlet;
import com.atlassian.plugin.spring.scanner.test.servlet.ManageDynamicContextServlet;

import static it.allproducts.ComponentExpectations.COMMON;
import static java.util.Collections.unmodifiableList;
import static org.junit.Assert.assertThat;

/**
 * Base class for integration tests of component-related functionality common to all products,
 * such as
 * {@link ComponentImport},
 * {@link ExportAsService},
 * and the dynamic context creation / profile support.
 * (i.e. everything that's not a product-specific component or import)
 *
 * See also product-specific subclasses.
 * See {@link it.allproducts.TestCustomModuleType}.
 */
public abstract class AbstractComponentsInProductTest extends AbstractInProductTest {

    /**
     * @see ComponentStatusServlet
     */
    private static final String COMPONENT_STATUS_URL = BASEURL + "/plugins/servlet/component-status?components";

    /**
     * @see ComponentStatusServlet
     */
    private static final String SERVICE_STATUS_URL = BASEURL + "/plugins/servlet/component-status?services";

    /**
     * @see ComponentStatusServlet
     */
    private static final String SPRING_STATUS_URL = BASEURL + "/plugins/servlet/spring-status";

    /**
     * @see ManageDynamicContextServlet
     */
    private static final String START_DYNAMIC_CONTEXT_URL =
            BASEURL + "/plugins/servlet/manage-dynamic-contexts?startup";

    private static final String STOP_DYNAMIC_CONTEXT_URL =
            BASEURL + "/plugins/servlet/manage-dynamic-contexts?shutdown";

    /**
     * Test that {@link Component}, {@link ComponentImport} etc. are working in atlassian-spring-scanner-maven-test.
     */
    @Test
    public void testComponents() throws Exception {
        List<String> actualComponentsInProduct = readStringList(COMPONENT_STATUS_URL);
        List<AbstractExpectedComponent> expected = getExpectedComponentsWithDefaultProfile();

        assertThat(actualComponentsInProduct, hasComponents(expected));
    }

    /**
     * Test that {@link ExportAsService} etc. are working in atlassian-spring-scanner-maven-test.
     */
    @Test
    public void testServices() throws Exception {
        List<String> actualServicesInProduct = readStringList(SERVICE_STATUS_URL);
        List<AbstractExpectedComponent> expected = getExpectedServiceExportsWithDefaultProfile();

        assertThat(actualServicesInProduct, hasComponents(expected));
    }

    /**
     * Test dynamic creation and shutdown of inner spring context (profiles)
     */
    @Test
    public void testDynamicContext() throws Exception {
        // Initially, we're running with the default profile, so we get the default profile's components and services
        // (same as other tests)

        List<AbstractExpectedComponent> expectedComponentsDefaultProfile = getExpectedComponentsWithDefaultProfile();
        List<AbstractExpectedComponent> expectedServicesDefaultProfile = getExpectedServiceExportsWithDefaultProfile();

        assertComponentsAndServices(expectedComponentsDefaultProfile, expectedServicesDefaultProfile);

        // Enable the dynamic context. This should load DynamicComponent.
        getUrl(START_DYNAMIC_CONTEXT_URL);

        // When the dynamic profile is activated, we expect a new component and a new import to appear.
        // No change in exported services.
        final List<AbstractExpectedComponent> expectedDynamicComponents = new ArrayList<>();
        expectedDynamicComponents.addAll(expectedComponentsDefaultProfile);
        expectedDynamicComponents.addAll(COMMON.getExpectedScannerCreatedDynamicComponents());
        expectedDynamicComponents.addAll(COMMON.getExpectedScannerCreatedDynamicComponentImports());

        assertComponentsAndServices(expectedDynamicComponents, expectedServicesDefaultProfile);

        // Stop the dynamic context - we should then return to the starting state - only default profile's components
        // and services
        getUrl(STOP_DYNAMIC_CONTEXT_URL);

        assertComponentsAndServices(expectedComponentsDefaultProfile, expectedServicesDefaultProfile);
    }

    private void assertComponentsAndServices(
            List<AbstractExpectedComponent> expectedComponents, List<AbstractExpectedComponent> expectedServices)
            throws IOException {
        List<String> actualComponents = readStringList(COMPONENT_STATUS_URL);
        List<String> actualServices = readStringList(SERVICE_STATUS_URL);

        assertThat(actualComponents, hasComponents(expectedComponents));
        assertThat(actualServices, hasComponents(expectedServices));
    }

    abstract ComponentExpectations getProductSpecificExpectations();

    private List<AbstractExpectedComponent> getExpectedComponentsWithDefaultProfile() {
        final ComponentExpectations productSpecificExpectations = getProductSpecificExpectations();

        final List<AbstractExpectedComponent> abstractExpectedComponents = new ArrayList<>();
        // Common to all products
        abstractExpectedComponents.addAll(
                productSpecificExpectations.getExpectedScannerCreatedComponents()); // includes those exported
        abstractExpectedComponents.addAll(productSpecificExpectations.getExpectedScannerCreatedComponentImports());
        abstractExpectedComponents.addAll(productSpecificExpectations.getExpectedAutoAddedComponents());
        // Product-specific
        abstractExpectedComponents.addAll(productSpecificExpectations.getProductSpecificScannerCreatedComponents());
        abstractExpectedComponents.addAll(
                productSpecificExpectations.getProductSpecificScannerCreatedComponentImports());
        abstractExpectedComponents.addAll(productSpecificExpectations.getProductSpecificAutoAddedComponents());

        if (isProductUsingSpring5()) {
            abstractExpectedComponents.addAll(productSpecificExpectations.getExpectedAutoAddedComponentsSpring5());
        } else {
            abstractExpectedComponents.addAll(productSpecificExpectations.getExpectedAutoAddedComponentsSpring());
        }

        return unmodifiableList(abstractExpectedComponents);
    }

    private boolean isProductUsingSpring5() {
        final String deprecatedRequiredAnnotationSpringVersion = "5.1.0";
        final String springVersion;
        try {
            springVersion = getUrl(SPRING_STATUS_URL);
        } catch (IOException e) {
            // we might be working with an older version of Spring Scanner that doesn't support the new servlet param,
            // so we will consider it is not Spring 5.
            return false;
        }

        final ComparableVersion comparableVersion = new ComparableVersion(springVersion);
        return comparableVersion.compareTo(new ComparableVersion(deprecatedRequiredAnnotationSpringVersion)) >= 0;
    }

    private List<AbstractExpectedComponent> getExpectedServiceExportsWithDefaultProfile() {
        final ComponentExpectations expectations = getProductSpecificExpectations();

        final List<AbstractExpectedComponent> expectedServiceExports = new ArrayList<>();
        expectedServiceExports.addAll(expectations.getExpectedScannerCreatedExports());
        expectedServiceExports.addAll(expectations.getExpectedAutoAddedExports());
        expectedServiceExports.addAll(expectations.getProductSpecificAutoExportedServices());
        // If we're running in dev mode (which is the AMPS default if this property isn't set), the dev exports should
        // be present, else not
        if (Boolean.parseBoolean(System.getProperty("atlassian.dev.mode", "true"))) {
            expectedServiceExports.addAll(expectations.getExpectedScannerCreatedDevExports());
        }
        return unmodifiableList(expectedServiceExports);
    }

    private static Matcher<Iterable<? extends String>> hasComponents(
            List<AbstractExpectedComponent> expectedComponents) {
        Collection<Matcher<? super String>> componentMatchers = expectedComponents.stream()
                .map(AbstractExpectedComponent::getRuntimeComponentEntryMatcher)
                .collect(Collectors.toList());
        return Matchers.containsInAnyOrder(componentMatchers);
    }
}
