package com.atlassian.plugin.spring.scanner.runtime.impl.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.spring.scanner.ProductFilter;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.ArrayUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.split;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

import static com.atlassian.plugin.spring.scanner.runtime.impl.util.ProductFilterUtil.getFilterForCurrentProduct;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.INDEX_FILES_DIR;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.PROFILE_PREFIX;

/**
 * A utility class to read index files from a classloader or bundle.
 * Can read a single index file, or concatenate multiple index files, based on the
 * currently running product.
 */
@ParametersAreNonnullByDefault
public final class AnnotationIndexReader {

    private static final Logger log = LoggerFactory.getLogger(AnnotationIndexReader.class);

    /**
     * Read a file from a bundle and return the list of lines of the file.
     *
     * @param resourceFile the path to the file in the bundle
     * @param bundle       the bundle from which to read
     * @return the lines in the file, or an empty list if it doesn't exist
     */
    public static List<String> readIndexFile(final String resourceFile, final Bundle bundle) {
        final URL url = bundle.getEntry(resourceFile);
        return readIndexFile(url);
    }

    /**
     * Reads both the cross-product file and the product-specific file for the currently
     * running product from the bundle and returns their entries as a single list.
     *
     * @param resourceFile the path to the cross-product file in the bundle
     * @param bundleContext the current bundle context
     * @return the lines in the file
     */
    public static List<String> readAllIndexFilesForProduct(
            final String resourceFile, final BundleContext bundleContext) {
        final Bundle bundle = bundleContext.getBundle();
        final URL resourceFileUrl = bundle.getEntry(resourceFile);
        final List<String> entries = new ArrayList<>(readIndexFile(resourceFileUrl));
        final ProductFilter filter = getFilterForCurrentProduct(bundleContext);
        if (filter != null) {
            // AS: The filter seems never to be null, but might very likely be ProductFilter.ALL
            entries.addAll(readIndexFile(filter.getPerProductFile(resourceFile), bundle));
        }
        return entries;
    }

    /**
     * Reads the entries from the given index file.
     *
     * @param indexFileUrl the URL of the file to read
     * @return an empty list if the URL is {@code null}
     */
    public static List<String> readIndexFile(@Nullable final URL indexFileUrl) {
        if (indexFileUrl == null) {
            log.debug("Could not find annotation index file (null url).");
            return emptyList();
        }
        final List<String> entries = readIndexFileLines(indexFileUrl);
        if (log.isDebugEnabled()) {
            log.debug("Successfully read annotation index file: {}", indexFileUrl);
            log.debug("Annotated beans: {}", entries);
        }
        return entries;
    }

    /**
     * Reads the Java properties file at the given URL.
     *
     * @param url the URL of the file to read
     * @return an empty properties object if the given URL is {@code null} or invalid
     */
    public static Properties readPropertiesFile(@Nullable final URL url) {
        if (url == null) {
            return new Properties();
        }
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), UTF_8))) {
            final Properties properties = new Properties();
            properties.load(reader);
            return properties;
        } catch (final FileNotFoundException e) {
            return new Properties();
        } catch (final IOException e) {
            throw new RuntimeIOException("Cannot read properties file [" + url + "]", e);
        }
    }

    /**
     * Splits the given comma-delimited list of profiles into an array.
     *
     * @param profiles the comma-delimited list
     * @return the array; empty if the input is {@code null} or blank
     */
    public static String[] splitProfiles(@Nullable final String profiles) {
        return split(trimToEmpty(profiles), ',');
    }

    /**
     * Returns the paths of the index files to read.
     *
     * @param profileNames any active profiles
     * @param indexFileName the name of the index file within each profile
     * @return a list of profile-specific filenames, or if no profiles were given, a single filename
     */
    public static List<String> getIndexFilesForProfiles(
            @Nullable final String[] profileNames, final String indexFileName) {
        if (isEmpty(profileNames)) {
            return singletonList(INDEX_FILES_DIR + "/" + indexFileName);
        }
        //noinspection ConstantConditions
        return stream(profileNames)
                .map(StringUtils::trimToEmpty)
                .filter(StringUtils::isNotEmpty)
                .map(profile -> getProfiledIndexFileName(profile, indexFileName))
                .collect(toList());
    }

    private static String getProfiledIndexFileName(final String profile, final String indexFileName) {
        return INDEX_FILES_DIR + "/" + PROFILE_PREFIX + profile + "/" + indexFileName;
    }

    private static List<String> readIndexFileLines(final URL indexFileUrl) {
        try (final Reader reader = new InputStreamReader(indexFileUrl.openStream(), UTF_8)) {
            return IOUtils.readLines(reader);
        } catch (final FileNotFoundException e) {
            log.debug("Could not find annotation index file {}", indexFileUrl);
            return emptyList();
        } catch (final IOException e) {
            throw new RuntimeIOException("Cannot read index file [" + indexFileUrl + "]", e);
        }
    }

    private AnnotationIndexReader() {
        throw new UnsupportedOperationException("Not for instantiation");
    }

    /**
     * Allows us not to throw generic runtime exceptions.
     */
    private static class RuntimeIOException extends RuntimeException {

        private RuntimeIOException(final String message, final Throwable cause) {
            super(message, cause);
        }
    }
}
