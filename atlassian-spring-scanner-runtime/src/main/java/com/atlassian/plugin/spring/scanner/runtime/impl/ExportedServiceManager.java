package com.atlassian.plugin.spring.scanner.runtime.impl;

import java.util.IdentityHashMap;
import java.util.Map;

import org.eclipse.gemini.blueprint.service.exporter.OsgiServiceRegistrationListener;
import org.eclipse.gemini.blueprint.service.exporter.support.DefaultInterfaceDetector;
import org.eclipse.gemini.blueprint.service.exporter.support.ExportContextClassLoaderEnum;
import org.eclipse.gemini.blueprint.service.exporter.support.OsgiServiceFactoryBean;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Collections.synchronizedMap;

/**
 * Utility class to encapsulate the registration/de-registration of OSGi services exported by public components
 */
public class ExportedServiceManager {

    private static final Logger log = LoggerFactory.getLogger(ExportedServiceManager.class);

    // Map each bean to its FactoryBean, by its identity (not equality)
    private final Map<Object, OsgiServiceFactoryBean> exporters = synchronizedMap(new IdentityHashMap<>());

    /**
     * Exports a component as an OSGi service for use by other bundles
     *
     * @param bundleContext the bundle context
     * @param bean          the bean to register
     * @param beanName      the name of that bean
     * @param serviceProps  the service properties
     * @param interfaces    the interface of that bean
     * @return the service registration handle
     * @throws Exception if things go badly
     */
    public ServiceRegistration registerService(
            final BundleContext bundleContext,
            final Object bean,
            final String beanName,
            final Map<String, Object> serviceProps,
            final Class<?>... interfaces)
            throws Exception {
        final OsgiServiceFactoryBean osgiExporter =
                createExporter(bundleContext, bean, beanName, serviceProps, interfaces);

        final OsgiServiceFactoryBean replacedBean = exporters.put(bean, osgiExporter);
        if (replacedBean != null) {
            log.warn("Tried to register the same instance of {} twice", replacedBean.getClass());
        }

        return osgiExporter.getObject();
    }

    /**
     * Query whether a bean was registered as a service with this exported service manager.
     *
     * @param bean the bean to query
     * @return true if bean was registered, false if not.
     */
    public boolean hasService(final Object bean) {
        return exporters.containsKey(bean);
    }

    /**
     * De-registers an OSGi service
     *
     * @param bundleContext the bundle context
     * @param bean          the bean to un-register
     */
    @SuppressWarnings("UnusedParameters")
    public void unregisterService(final BundleContext bundleContext, final Object bean) {
        final OsgiServiceFactoryBean exporter = exporters.remove(bean);
        if (exporter != null) {
            exporter.destroy();
        }
    }

    /**
     * creates the OsgiServiceFactoryBean used by spring when registering services
     */
    private OsgiServiceFactoryBean createExporter(
            final BundleContext bundleContext,
            final Object bean,
            final String beanName,
            final Map<String, Object> serviceProps,
            final Class<?>[] interfaces)
            throws Exception {
        serviceProps.put("org.eclipse.gemini.blueprint.bean.name", beanName);

        final OsgiServiceFactoryBean exporter = new OsgiServiceFactoryBean();
        exporter.setInterfaceDetector(DefaultInterfaceDetector.DISABLED);
        exporter.setBeanClassLoader(bean.getClass().getClassLoader());
        exporter.setBeanName(beanName);
        exporter.setBundleContext(bundleContext);
        exporter.setExportContextClassLoader(ExportContextClassLoaderEnum.UNMANAGED);
        exporter.setInterfaces(interfaces);
        exporter.setServiceProperties(serviceProps);
        exporter.setTarget(bean);
        exporter.setListeners(new OsgiServiceRegistrationListener[0]);

        exporter.afterPropertiesSet();

        return exporter;
    }
}
