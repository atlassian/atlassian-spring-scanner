package com.atlassian.plugin.spring.scanner.runtime.impl;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.annotation.ParametersAreNonnullByDefault;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.eclipse.gemini.blueprint.service.importer.support.OsgiServiceProxyFactoryBean;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.osgi.factory.OsgiPlugin;

import static java.beans.Introspector.decapitalize;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static org.apache.commons.lang3.StringUtils.defaultIfBlank;
import static org.springframework.beans.factory.config.AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR;
import static org.springframework.beans.factory.config.BeanDefinition.ROLE_INFRASTRUCTURE;
import static org.springframework.beans.factory.support.BeanDefinitionBuilder.rootBeanDefinition;

import static com.atlassian.plugin.spring.scanner.runtime.impl.util.AnnotationIndexReader.getIndexFilesForProfiles;
import static com.atlassian.plugin.spring.scanner.runtime.impl.util.AnnotationIndexReader.readAllIndexFilesForProduct;
import static com.atlassian.plugin.spring.scanner.runtime.impl.util.AnnotationIndexReader.splitProfiles;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_IMPORT_KEY;

/**
 * A Spring {@link BeanFactoryPostProcessor} that generates a Spring proxy bean for each type of service annotated with
 * one of Spring Scanner's {@code *Import} annotations. It runs after all of the bean definitions have been gathered for
 * the current bundle. This is a BeanFactoryPostProcessor because it needs to run before the beans are created, so that
 * the imported services are available to be injected into the plugin's own beans.
 */
@SuppressWarnings("UnusedDeclaration")
@ParametersAreNonnullByDefault
public class ComponentImportBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ComponentImportBeanFactoryPostProcessor.class);

    private final BundleContext bundleContext;

    private String profileName; // setter-injected

    public ComponentImportBeanFactoryPostProcessor(final BundleContext bundleContext) {
        this.bundleContext = requireNonNull(bundleContext);
    }

    public void setProfileName(final String profileName) {
        this.profileName = profileName;
    }

    /**
     * Reads the component import index file(s) and registers the bean wrappers that represent imported OSGi services.
     *
     * @param beanFactory the bean factory
     */
    @Override
    public void postProcessBeanFactory(final ConfigurableListableBeanFactory beanFactory) {
        final Collection<String> importedServices = readServicesToImportFromSpringScannerIndexFiles();
        final BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;
        generateOsgiServiceImportBeans(beanFactory.getBeanClassLoader(), importedServices)
                .forEach(registry::registerBeanDefinition);
    }

    /**
     * Parses the relevant Spring Scanner index file(s) to find out the OSGi services to import. The Spring Scanner
     * Maven plugin creates these index files (among others) at build time, based on the usage of annotations from the
     * {@code com.atlassian.plugin.spring.scanner.annotation.imports} package, typically
     * {@link com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport}.
     *
     * @return a collection of unique Strings of the form {@code <bean_class>[#<bean_name>]}
     */
    private Collection<String> readServicesToImportFromSpringScannerIndexFiles() {
        final String[] profileNames = splitProfiles(profileName);
        final Collection<String> indexFileEntries = new TreeSet<>();
        for (final String fileToRead : getIndexFilesForProfiles(profileNames, COMPONENT_IMPORT_KEY)) {
            indexFileEntries.addAll(readAllIndexFilesForProduct(fileToRead, bundleContext));
        }
        return indexFileEntries;
    }

    /**
     * Generates the definitions of proxy beans that import the given OSGi services.
     *
     * @param serviceClassLoader the classloader for loading the service interfaces
     * @param indexFileEntries the class name plus optional bean name (#-delimited) of each service to import, for
     *                         example {@code com.foo.Bar#myBeanName}
     * @return a map of Spring bean names to their definitions
     */
    static Map<String, BeanDefinition> generateOsgiServiceImportBeans(
            final ClassLoader serviceClassLoader, final Iterable<String> indexFileEntries) {
        final Map<String, BeanDefinition> beanDefinitions = new TreeMap<>();
        for (final String indexFileEntry : indexFileEntries) {
            final String[] typeAndName = indexFileEntry.split("#");
            final String serviceClassName = typeAndName[0];
            loadServiceClass(serviceClassName, serviceClassLoader).ifPresent(serviceClass -> {
                final String userProvidedBeanName = (typeAndName.length > 1) ? typeAndName[1] : null;
                final String beanName = defaultIfBlank(
                        userProvidedBeanName, getDefaultBeanName(serviceClass, beanDefinitions.keySet()));
                final BeanDefinition beanDefinition = generateOsgiServiceImportBean(serviceClass);
                beanDefinitions.put(beanName, beanDefinition);
            });
        }
        return beanDefinitions;
    }

    private static String getDefaultBeanName(final Class<?> serviceClass, final Collection<String> existingBeanNames) {
        final String firstAttempt = decapitalize(serviceClass.getSimpleName()); // preserve original convention if poss
        if (!existingBeanNames.contains(firstAttempt)) {
            return firstAttempt;
        }
        return serviceClass.getName();
    }

    private static Optional<Class<?>> loadServiceClass(
            final String serviceClass, final ClassLoader serviceClassLoader) {
        try {
            return Optional.of(serviceClassLoader.loadClass(serviceClass));
        } catch (final ClassNotFoundException e) {
            LOGGER.warn("Unable to load class '{}' for component importation purposes. Skipping...", serviceClass);
            return empty();
        }
    }

    /**
     * Generates the definition of a proxy bean that imports the given OSGi service.
     *
     * @param serviceClass the interface of the imported service
     * @return the generated bean definition
     */
    static BeanDefinition generateOsgiServiceImportBean(final Class<?> serviceClass) {
        final BeanDefinitionBuilder builder = rootBeanDefinition(OsgiServiceProxyFactoryBean.class);
        builder.setAutowireMode(AUTOWIRE_CONSTRUCTOR);
        builder.setRole(ROLE_INFRASTRUCTURE);
        builder.addPropertyValue("beanClassLoader", OsgiPlugin.class.getClassLoader());
        builder.addPropertyValue("filter", "(objectClass=" + serviceClass.getName() + ")");
        builder.addPropertyValue("interfaces", new Class<?>[] {serviceClass});
        return builder.getBeanDefinition();
    }
}
