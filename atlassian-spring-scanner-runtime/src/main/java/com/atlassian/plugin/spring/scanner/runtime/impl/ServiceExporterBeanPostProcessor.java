package com.atlassian.plugin.spring.scanner.runtime.impl;

import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.aop.support.AopUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.annotation.export.ServiceProperty;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toMap;

import static com.atlassian.plugin.spring.scanner.runtime.impl.util.AnnotationIndexReader.getIndexFilesForProfiles;
import static com.atlassian.plugin.spring.scanner.runtime.impl.util.AnnotationIndexReader.readAllIndexFilesForProduct;
import static com.atlassian.plugin.spring.scanner.runtime.impl.util.AnnotationIndexReader.splitProfiles;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_DEV_EXPORT_KEY;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_EXPORT_KEY;

/**
 * A BeanPostProcessor that exports OSGi services for beans annotated with both a *Component annotation and the
 * ExportAsService annotation This essentially does the same thing as the "public=true" on an atlassian-plugin.xml
 * component entry.
 * <p>
 * This is implemented as a BeanPostProcessor because we need to service to come and go as the bean is
 * created/destroyed
 */
public class ServiceExporterBeanPostProcessor implements DestructionAwareBeanPostProcessor, InitializingBean {

    public static final String OSGI_SERVICE_SUFFIX = "_osgiService";

    static final String ATLASSIAN_DEV_MODE_PROP = "atlassian.dev.mode";

    private static final Logger log = LoggerFactory.getLogger(ServiceExporterBeanPostProcessor.class);

    private final boolean isDevMode = Boolean.parseBoolean(System.getProperty(ATLASSIAN_DEV_MODE_PROP, "false"));
    private final BundleContext bundleContext;
    private final ConfigurableListableBeanFactory beanFactory;
    private final ExportedServiceManager serviceManager;
    private final Map<String, String[]> exports;

    private String profileName;

    public ServiceExporterBeanPostProcessor(
            final BundleContext bundleContext, final ConfigurableListableBeanFactory beanFactory) {
        this(bundleContext, beanFactory, new ExportedServiceManager());
    }

    ServiceExporterBeanPostProcessor(
            final BundleContext bundleContext,
            final ConfigurableListableBeanFactory beanFactory,
            ExportedServiceManager serviceManager) {
        this.bundleContext = bundleContext;
        this.beanFactory = beanFactory;
        this.exports = new HashMap<>();
        this.profileName = null;
        this.serviceManager = serviceManager;
    }

    // Used to inject profileName
    @SuppressWarnings("UnusedDeclaration")
    public void setProfileName(final String profileName) {
        this.profileName = profileName;
    }

    @Override
    public void afterPropertiesSet() {
        final String[] profileNames = splitProfiles(profileName);
        parseExportsForExportFile(COMPONENT_EXPORT_KEY, profileNames);
        if (isDevMode) {
            parseExportsForExportFile(COMPONENT_DEV_EXPORT_KEY, profileNames);
        }
    }

    private void parseExportsForExportFile(final String exportFileName, final String[] profileNames) {
        final String[] defaultInterfaces = {};
        for (final String fileToRead : getIndexFilesForProfiles(profileNames, exportFileName)) {
            final List<String> exportData = readAllIndexFilesForProduct(fileToRead, bundleContext);
            for (final String export : exportData) {
                final String[] targetAndInterfaces = export.split("#");
                final String target = targetAndInterfaces[0];
                final String[] interfaces =
                        (targetAndInterfaces.length > 1) ? targetAndInterfaces[1].split(",") : defaultInterfaces;
                exports.put(target, interfaces);
            }
        }
    }

    @Override
    public void postProcessBeforeDestruction(final Object bean, final String beanName) {
        if (serviceManager.hasService(bean)) {
            serviceManager.unregisterService(bundleContext, bean);

            final String serviceName = getServiceName(beanName);
            if (beanFactory.containsBean(serviceName)) {
                final Object serviceBean = beanFactory.getBean(serviceName);

                if (null != serviceBean) {
                    beanFactory.destroyBean(serviceName, serviceBean);
                }
            }
        }
    }

    @Override
    public Object postProcessBeforeInitialization(final Object bean, final String beanName) {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(final Object bean, final String beanName) {
        Class<?>[] interfaces = {};
        ServiceProperty[] serviceProperties = {};
        // If the actual bean is some proxy object because of Spring AOP, this extracts the actual declared class of the
        // bean.
        // The answer will only be different from getClass() when AOP is in play - in such case this will retrieve
        // the real class used in the code ignoring AOP proxy
        final Class<?> beanTargetClass = AopUtils.getTargetClass(bean);
        final String beanClassName = beanTargetClass.getName();

        if (exports.containsKey(beanClassName) || isPublicComponent(beanTargetClass)) {
            if (exports.containsKey(beanClassName)) {
                interfaces = getExportedInterfaces(bean, beanName, beanClassName);
            } else if (hasAnnotation(beanTargetClass, ModuleType.class)) {
                interfaces = beanTargetClass.getAnnotation(ModuleType.class).value();
                serviceProperties =
                        beanTargetClass.getAnnotation(ModuleType.class).properties();
            } else if (hasAnnotation(beanTargetClass, ExportAsService.class)) {
                interfaces =
                        beanTargetClass.getAnnotation(ExportAsService.class).value();
                serviceProperties =
                        beanTargetClass.getAnnotation(ExportAsService.class).properties();
            } else if (hasAnnotation(beanTargetClass, ExportAsDevService.class)) {
                interfaces =
                        beanTargetClass.getAnnotation(ExportAsDevService.class).value();
                serviceProperties =
                        beanTargetClass.getAnnotation(ExportAsDevService.class).properties();
            }

            // if they didn't specify any interfaces, calculate them
            if (interfaces.length < 1) {
                // This only gets direct interfaces, not inherited ones, which is documented behaviour.
                // It's desireable because it reduces brittleness with respect to superclass changes.
                interfaces = beanTargetClass.getInterfaces();

                // if we still don't have any, just export with the classname (yes, OSGi allows this.
                if (interfaces.length < 1) {
                    interfaces = new Class<?>[] {beanTargetClass};
                }
            }

            try {
                final ServiceRegistration serviceRegistration = serviceManager.registerService(
                        bundleContext, bean, beanName, asMap(serviceProperties), interfaces);
                final String serviceName = getServiceName(beanName);
                beanFactory.initializeBean(serviceRegistration, serviceName);
            } catch (final Exception e) {
                log.error("Unable to register bean '{}' as an OSGi exported service", beanName, e);
            }
        }

        return bean;
    }

    private Class<?>[] getExportedInterfaces(final Object bean, final String beanName, final String beanClassName) {
        // We need to turn the string interface names from the exports file into actual Class objects. We do this by
        // asking the bean's class loader for each interface by name. We could walk the class hierarchy looking for
        // the ones we want, but that seems a bit more roundabout.
        final ClassLoader beanClassLoader = bean.getClass().getClassLoader();
        return stream(exports.get(beanClassName))
                .flatMap(interfaceName -> loadClass(interfaceName, beanClassLoader, beanName))
                .toArray(Class<?>[]::new);
    }

    private static Stream<Class<?>> loadClass(
            final String name, final ClassLoader beanClassLoader, final String beanName) {
        try {
            return Stream.of(beanClassLoader.loadClass(name));
        } catch (final ClassNotFoundException e) {
            log.warn("Cannot find class for export '{}' of bean '{}': ", name, beanName, e);
            // And drop it - there's not much else we can do
            return Stream.empty();
        }
    }

    private Map<String, Object> asMap(final ServiceProperty[] properties) {
        return stream(properties).collect(toMap(ServiceProperty::key, ServiceProperty::value));
    }

    private boolean isPublicComponent(final Class<?> beanTargetClass) {
        return hasAnnotation(beanTargetClass, ModuleType.class)
                || hasAnnotation(beanTargetClass, ExportAsService.class)
                || (hasAnnotation(beanTargetClass, ExportAsDevService.class) && isDevMode);
    }

    private boolean hasAnnotation(final Class<?> beanTargetClass, final Class<? extends Annotation> annotationClass) {
        return beanTargetClass.isAnnotationPresent(annotationClass);
    }

    private String getServiceName(final String beanName) {
        return beanName + OSGI_SERVICE_SUFFIX;
    }
}
