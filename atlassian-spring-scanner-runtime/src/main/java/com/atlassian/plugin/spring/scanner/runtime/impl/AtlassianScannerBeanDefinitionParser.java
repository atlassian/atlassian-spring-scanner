package com.atlassian.plugin.spring.scanner.runtime.impl;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.parsing.BeanComponentDefinition;
import org.springframework.beans.factory.parsing.CompositeComponentDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.springframework.beans.factory.xml.XmlReaderContext;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.ClassUtils;
import org.eclipse.gemini.blueprint.context.ConfigurableOsgiBundleApplicationContext;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static org.springframework.beans.factory.support.AbstractBeanDefinition.AUTOWIRE_CONSTRUCTOR;
import static org.springframework.beans.factory.xml.BeanDefinitionParserDelegate.AUTOWIRE_ATTRIBUTE;
import static org.springframework.context.annotation.AnnotationConfigUtils.registerAnnotationConfigProcessors;

/**
 * This class is responsible for handling the "parsing" of the scan-indexes element in the spring beans file.
 * Ultimately, this is what kicks off the index scanner and is the starting point for registering bean definitions
 */
public class AtlassianScannerBeanDefinitionParser implements BeanDefinitionParser {
    private static final String PROFILE_ATTRIBUTE = "profile";
    public static final String JAVAX_INJECT_CLASSNAME = "javax.inject.Inject";

    private static final Logger log = LoggerFactory.getLogger(AtlassianScannerBeanDefinitionParser.class);

    @Override
    public BeanDefinition parse(final Element element, final ParserContext parserContext) {
        String profileName = null;
        Integer autowireDefault = null;
        if (element.hasAttribute(PROFILE_ATTRIBUTE)) {
            profileName = element.getAttribute(PROFILE_ATTRIBUTE);
        }
        if (element.hasAttribute(AUTOWIRE_ATTRIBUTE)) {
            // Pass the AUTOWIRE_ATTRIBUTE to the delegate for parsing.
            autowireDefault = parserContext.getDelegate().getAutowireMode(element.getAttribute(AUTOWIRE_ATTRIBUTE));
        }

        // Actually scan for bean definitions and register them.

        final BundleContext targetPluginBundleContext = getBundleContext(parserContext);

        checkScannerRuntimeIsNotEmbeddedInBundle(targetPluginBundleContext);

        final ClassIndexBeanDefinitionScanner scanner = new ClassIndexBeanDefinitionScanner(
                parserContext.getReaderContext().getRegistry(),
                profileName,
                autowireDefault,
                targetPluginBundleContext);
        final Set<BeanDefinitionHolder> beanDefinitions = scanner.doScan();

        registerComponents(parserContext.getReaderContext(), beanDefinitions, element, profileName);

        return null;
    }

    /**
     * Takes the scanned bean definitions and adds them to a root component. Also adds in the post-processors required
     * to import/export OSGi services. Finally fires the component registered event with our root component.
     *
     * @param readerContext   the xml context
     * @param beanDefinitions the set of bean definitions
     * @param element         the xml element of definition
     * @param profileName     the name of the profile to read definitions from
     */
    protected void registerComponents(
            final XmlReaderContext readerContext,
            final Set<BeanDefinitionHolder> beanDefinitions,
            final Element element,
            final String profileName) {
        final Object source = readerContext.extractSource(element);
        final CompositeComponentDefinition compositeDef =
                new CompositeComponentDefinition(element.getTagName(), source);

        // Add the beans we found to our root component
        for (final BeanDefinitionHolder beanDefHolder : beanDefinitions) {
            compositeDef.addNestedComponent(new BeanComponentDefinition(beanDefHolder));
        }

        // add our custom post-processors along with the standard @Autowired processor
        final Set<BeanDefinitionHolder> processorDefinitions =
                new LinkedHashSet<>(registerAnnotationConfigProcessors(readerContext.getRegistry(), source));

        // Let's be nice and support javax.inject.Inject annotations
        final BeanDefinitionHolder javaxInject = getJavaxInjectPostProcessor(readerContext.getRegistry(), source);
        if (null != javaxInject) {
            processorDefinitions.add(javaxInject);
        }
        processorDefinitions.add(getComponentImportPostProcessor(readerContext.getRegistry(), source, profileName));
        processorDefinitions.add(getServiceExportPostProcessor(readerContext.getRegistry(), source, profileName));
        processorDefinitions.add(getDevModeBeanInitialisationLoggerPostProcessor(readerContext.getRegistry(), source));

        for (final BeanDefinitionHolder processorDefinition : processorDefinitions) {
            compositeDef.addNestedComponent(new BeanComponentDefinition(processorDefinition));
        }

        readerContext.fireComponentRegistered(compositeDef);
    }

    /**
     * Get the plugin BundleContext for the spring application context we're initializing,
     * by casting the ResourceLoader to the gemini ConfigurableOsgiBundleApplicationContext class.
     * <p>
     * This is necessary because AtlassianScannerNamespaceHandler has to have a no-arg constructor
     * so can't get a BundleContext by constructor injection like ComponentImportBeanFactoryPostProcessor does.
     */
    private BundleContext getBundleContext(ParserContext parserContext) {
        ResourceLoader resourceLoader = parserContext.getReaderContext().getResourceLoader();

        if (!(resourceLoader instanceof ConfigurableOsgiBundleApplicationContext)) {
            throw new IllegalStateException("Could not access BundleContext from ResourceLoader: "
                    + "expected resourceLoader to be an instance of "
                    + ConfigurableOsgiBundleApplicationContext.class.getName() + ": got "
                    + resourceLoader.getClass().getName());
        }

        final BundleContext bundleContext =
                ((ConfigurableOsgiBundleApplicationContext) resourceLoader).getBundleContext();
        if (bundleContext == null) {
            throw new IllegalStateException("Could not access BundleContext from ResourceLoader: "
                    + "ConfigurableOsgiBundleApplicationContext.getBundleContext returned null");
        }

        return bundleContext;
    }

    /**
     * See SCANNER-17, SCANNER-54, SCANNER-57.
     * Only allow scanner-runtime to work when used with 'provided' scope; refuse to work if embedded in a plugin
     * with 'runtime' scope.
     * This means we know there's only one copy in the system - the 'provided' bundle,
     * hence we avoid the SCANNER-54 issue where gemini's namespace handling finds copies that aren't ready to execute.
     */
    private void checkScannerRuntimeIsNotEmbeddedInBundle(BundleContext targetPluginBundleContext) {
        final Bundle pluginInvokingScannerRuntime = targetPluginBundleContext.getBundle();
        if (pluginInvokingScannerRuntime == null) {
            throw new IllegalStateException(
                    "Cannot execute atlassian-spring-scanner-runtime from a plugin that is not in a valid state: "
                            + "bundleContext.getBundle() returned null for plugin bundle.");
        }

        final String howToFixScope = "Use 'mvn dependency:tree' and ensure the atlassian-spring-scanner-annotation "
                + "dependency in your plugin has <scope>provided</scope>, not 'runtime' or 'compile', "
                + "and you have NO dependency on atlassian-spring-scanner-runtime.";

        final Bundle bundleContainingThisScannerRuntime =
                FrameworkUtil.getBundle(AtlassianScannerBeanDefinitionParser.class);
        if (bundleContainingThisScannerRuntime == null) {
            throw new IllegalStateException("Incorrect use of atlassian-spring-scanner-runtime: "
                    + "atlassian-spring-scanner-runtime classes do not appear to be coming from a bundle classloader. "
                    + howToFixScope);
        }
        final Bundle bundleContainingScannerAnnotationLibsAsSeenByRuntime =
                FrameworkUtil.getBundle(ComponentImport.class);
        if (bundleContainingScannerAnnotationLibsAsSeenByRuntime == null) {
            throw new IllegalStateException("Incorrect use of atlassian-spring-scanner-runtime: "
                    + "atlassian-spring-scanner-annotation classes do not appear to be coming from a bundle classloader. "
                    + howToFixScope);
        }

        final long invokingPluginBundleId = pluginInvokingScannerRuntime.getBundleId();
        final long scannerRuntimeBundleId = bundleContainingThisScannerRuntime.getBundleId();
        if (invokingPluginBundleId == scannerRuntimeBundleId) {
            throw new IllegalStateException("Incorrect use of atlassian-spring-scanner-runtime: "
                    + "atlassian-spring-scanner-runtime classes are embedded inside the target plugin '"
                    + pluginInvokingScannerRuntime.getSymbolicName()
                    + "'; embedding scanner-runtime is not supported since scanner version 2.0. "
                    + howToFixScope);
        }

        // Also check whether there's an embedded copy of the scanner-annotation classes.
        // This will PROBABLY still work (likely, scanner-runtime will ignore it and use the scanner-annotation bundle)
        // but better to make sure people get their packaging right when they switch to 2.0-provided;
        // will aid debugging etc.
        Bundle bundleContainingScannerAnnotationLibsAsSeenByPlugin;
        try {
            bundleContainingScannerAnnotationLibsAsSeenByPlugin =
                    FrameworkUtil.getBundle(pluginInvokingScannerRuntime.loadClass(ComponentImport.class.getName()));
        } catch (ClassNotFoundException e) {
            // Plugin doesn't import the annotation classes, and also doesn't embed them.
            // Weird, but no problem. Runtime still works.
            return;
        }
        final long scannerAnnotationBundleId = bundleContainingScannerAnnotationLibsAsSeenByRuntime.getBundleId();
        if (bundleContainingScannerAnnotationLibsAsSeenByPlugin == null
                || bundleContainingScannerAnnotationLibsAsSeenByPlugin.getBundleId() != scannerAnnotationBundleId) {
            throw new IllegalStateException("Cannot execute atlassian-spring-scanner-runtime: "
                    + "plugin has an extra copy of atlassian-spring-scanner-annotation classes, perhaps embedded inside the target plugin '"
                    + pluginInvokingScannerRuntime.getSymbolicName()
                    + "'; embedding scanner-annotations is not supported since scanner version 2.0. "
                    + howToFixScope);
        }
    }

    private BeanDefinitionHolder getJavaxInjectPostProcessor(
            final BeanDefinitionRegistry registry, final Object source) {
        if (ClassUtils.isPresent(JAVAX_INJECT_CLASSNAME, getClass().getClassLoader())) {
            try {
                final Class injectClass = getClass().getClassLoader().loadClass(JAVAX_INJECT_CLASSNAME);
                final Map<String, Object> properties = new HashMap<String, Object>();
                properties.put("autowiredAnnotationType", injectClass);

                final RootBeanDefinition def = new RootBeanDefinition(AutowiredAnnotationBeanPostProcessor.class);
                def.setSource(source);
                def.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
                def.setPropertyValues(new MutablePropertyValues(properties));

                return registerBeanPostProcessor(registry, def, "javaxInjectBeanPostProcessor");
            } catch (final ClassNotFoundException e) {
                log.error("Unable to load class '" + JAVAX_INJECT_CLASSNAME
                        + "' for javax component purposes.  Not sure how this is possible.  Skipping...");
            }
        }

        return null;
    }

    /**
     * Helper to convert a post-processor into a proper holder
     *
     * @param registry   the registry of bean defs
     * @param definition the root definition
     * @param beanName   the name of the bean to register
     * @return a bean def holder
     */
    private BeanDefinitionHolder registerBeanPostProcessor(
            final BeanDefinitionRegistry registry, final RootBeanDefinition definition, final String beanName) {
        definition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);

        registry.registerBeanDefinition(beanName, definition);
        return new BeanDefinitionHolder(definition, beanName);
    }

    private BeanDefinitionHolder getComponentImportPostProcessor(
            final BeanDefinitionRegistry registry, final Object source, final String profileName) {
        // Note that we need a null capable collection here
        final Map<String, Object> properties = new HashMap<String, Object>();
        properties.put("profileName", profileName);

        final RootBeanDefinition rootBeanDefinition =
                new RootBeanDefinition(ComponentImportBeanFactoryPostProcessor.class);
        rootBeanDefinition.setAutowireMode(AUTOWIRE_CONSTRUCTOR);
        rootBeanDefinition.setSource(source);
        rootBeanDefinition.setPropertyValues(new MutablePropertyValues(properties));

        return registerBeanPostProcessor(registry, rootBeanDefinition, "componentImportBeanFactoryPostProcessor");
    }

    private BeanDefinitionHolder getServiceExportPostProcessor(
            final BeanDefinitionRegistry registry, final Object source, final String profileName) {
        // Note that we need a null capable collection here
        final HashMap<String, Object> properties = new HashMap<String, Object>();
        properties.put("profileName", profileName);

        final RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(ServiceExporterBeanPostProcessor.class);
        rootBeanDefinition.setSource(source);
        rootBeanDefinition.setAutowireMode(AUTOWIRE_CONSTRUCTOR);
        rootBeanDefinition.setPropertyValues(new MutablePropertyValues(properties));

        return registerBeanPostProcessor(registry, rootBeanDefinition, "serviceExportBeanPostProcessor");
    }

    private BeanDefinitionHolder getDevModeBeanInitialisationLoggerPostProcessor(
            final BeanDefinitionRegistry registry, final Object source) {
        final RootBeanDefinition def = new RootBeanDefinition(DevModeBeanInitialisationLoggerBeanPostProcessor.class);
        def.setSource(source);
        def.setAutowireMode(AUTOWIRE_CONSTRUCTOR);

        return registerBeanPostProcessor(registry, def, "devModeBeanInitialisationLoggerBeanPostProcessor");
    }
}
