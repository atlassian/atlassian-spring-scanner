package com.atlassian.plugin.spring.scanner.runtime.impl.util;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import javax.annotation.Nullable;

import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.plugin.spring.scanner.ProductFilter;

/**
 * Utility to figure out the currently running product and give us its name/filter in various ways.
 */
public final class ProductFilterUtil {

    private static ProductFilterUtil instance;

    static final String CLASS_ON_BAMBOO_CLASSPATH = "com.atlassian.bamboo.build.BuildExecutionManager";
    static final String CLASS_ON_BITBUCKET_CLASSPATH = "com.atlassian.bitbucket.repository.RepositoryService";
    static final String CLASS_ON_CONFLUENCE_CLASSPATH = "com.atlassian.confluence.core.ContentEntityManager";
    static final String CLASS_ON_FECRU_CLASSPATH = "com.atlassian.fisheye.spi.services.RepositoryService";
    static final String CLASS_ON_JIRA_CLASSPATH = "com.atlassian.jira.bc.issue.IssueService";
    static final String CLASS_ON_REFAPP_CLASSPATH = "com.atlassian.refapp.api.ConnectionProvider";
    static final String CLASS_ON_CROWD_CLASSPATH = "com.atlassian.crowd.manager.mail.MailManager";

    private static final Logger log = LoggerFactory.getLogger(ProductFilterUtil.class);

    private final AtomicReference<ProductFilter> filterForProduct = new AtomicReference<>();

    private ProductFilterUtil() {}

    /**
     * @param bundleContext the bundle context
     * @return the ProductFilter instance that represents the currently running product.
     */
    public static ProductFilter getFilterForCurrentProduct(@Nullable final BundleContext bundleContext) {
        return getInstance().getFilterForProduct(bundleContext);
    }

    public ProductFilter getFilterForProduct(@Nullable final BundleContext bundleContext) {
        // lazy building of known product.
        ProductFilter productFilter = filterForProduct.get();
        if (productFilter == null) {
            filterForProduct.compareAndSet(productFilter, detectProduct(bundleContext));
            productFilter = filterForProduct.get();
        }
        return productFilter;
    }

    /**
     * We try to detect a OSGi service that is uniquely offered by the host.
     * <p/>
     * You should extend out this method as we add support for other products
     */
    private static final Map<String, ProductFilter> PRODUCTS_TO_HOST_CLASSES = new HashMap<>();

    static {
        PRODUCTS_TO_HOST_CLASSES.put(CLASS_ON_BAMBOO_CLASSPATH, ProductFilter.BAMBOO);
        PRODUCTS_TO_HOST_CLASSES.put(CLASS_ON_BITBUCKET_CLASSPATH, ProductFilter.BITBUCKET);
        PRODUCTS_TO_HOST_CLASSES.put(CLASS_ON_CONFLUENCE_CLASSPATH, ProductFilter.CONFLUENCE);
        PRODUCTS_TO_HOST_CLASSES.put(CLASS_ON_FECRU_CLASSPATH, ProductFilter.FECRU);
        PRODUCTS_TO_HOST_CLASSES.put(CLASS_ON_JIRA_CLASSPATH, ProductFilter.JIRA);
        PRODUCTS_TO_HOST_CLASSES.put(CLASS_ON_REFAPP_CLASSPATH, ProductFilter.REFAPP);
        PRODUCTS_TO_HOST_CLASSES.put(CLASS_ON_CROWD_CLASSPATH, ProductFilter.CROWD);
    }

    private ProductFilter detectProduct(final BundleContext bundleContext) {
        if (bundleContext == null) {
            log.warn("Couldn't detect product due to null bundleContext: will use ProductFilter.ALL");
            return ProductFilter.ALL;
        }

        for (Map.Entry<String, ProductFilter> entry : PRODUCTS_TO_HOST_CLASSES.entrySet()) {
            if (detectService(bundleContext, entry.getKey())) {
                if (log.isDebugEnabled()) {
                    log.debug("Detected product: {}", entry.getValue());
                }
                return entry.getValue();
            }
        }

        log.warn("Couldn't detect product, no known services found: will use ProductFilter.ALL");
        return ProductFilter.ALL;
    }

    private boolean detectService(final BundleContext bundleContext, final String serviceClassName) {
        ServiceReference<?> serviceReference = null;
        try {
            // We don't actually need to get the service, we just check we can get a reference
            serviceReference = bundleContext.getServiceReference(serviceClassName);
            return serviceReference != null;
        } finally {
            if (serviceReference != null) {
                bundleContext.ungetService(serviceReference);
            }
        }
    }

    private static ProductFilterUtil getInstance() {
        if (instance == null) {
            instance = new ProductFilterUtil();
        }
        return instance;
    }
}
