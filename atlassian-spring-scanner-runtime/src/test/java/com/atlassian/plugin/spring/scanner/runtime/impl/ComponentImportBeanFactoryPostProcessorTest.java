package com.atlassian.plugin.spring.scanner.runtime.impl;

import java.util.Map;

import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.config.BeanDefinition;
import org.eclipse.gemini.blueprint.service.importer.support.OsgiServiceProxyFactoryBean;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.junit.MockitoJUnit;

import com.atlassian.plugin.osgi.factory.OsgiPlugin;

import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.springframework.beans.factory.config.BeanDefinition.ROLE_INFRASTRUCTURE;

import static com.atlassian.plugin.spring.scanner.runtime.impl.ComponentImportBeanFactoryPostProcessor.generateOsgiServiceImportBean;
import static com.atlassian.plugin.spring.scanner.runtime.impl.ComponentImportBeanFactoryPostProcessor.generateOsgiServiceImportBeans;

public class ComponentImportBeanFactoryPostProcessorTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    // Easier than mocking, we just need to confine the test to loading classes visible to *this* class
    private static final ClassLoader SERVICE_CLASS_LOADER =
            ComponentImportBeanFactoryPostProcessorTest.class.getClassLoader();

    @Test
    public void generateOsgiServiceImportBeans_whenGivenNoEntries_shouldReturnEmptyMap() {
        // Act
        final Map<String, BeanDefinition> beanDefinitions =
                generateOsgiServiceImportBeans(SERVICE_CLASS_LOADER, emptySet());

        // Assert
        assertThat(beanDefinitions, is(emptyMap()));
    }

    @Test
    public void generateOsgiServiceImportBean_shouldGenerateExpectedBeanDefinition() {
        // Arrange
        final Class<?> serviceClass = TestService.class;

        // Act
        final BeanDefinition beanDefinition = generateOsgiServiceImportBean(serviceClass);

        // Assert
        assertThat(beanDefinition.getBeanClassName(), is(OsgiServiceProxyFactoryBean.class.getName()));
        assertThat(beanDefinition.getRole(), is(ROLE_INFRASTRUCTURE));
        assertTrue(beanDefinition.isAutowireCandidate());
        final MutablePropertyValues propertyValues = beanDefinition.getPropertyValues();
        assertThat(propertyValues.get("beanClassLoader"), is(OsgiPlugin.class.getClassLoader()));
        assertThat(propertyValues.get("filter"), is(format("(objectClass=%s)", serviceClass.getName())));
        assertThat(propertyValues.get("interfaces"), is(new Class<?>[] {TestService.class}));
    }

    @Test
    public void generateOsgiServiceImportBeans_whenNoBeanNameGiven_shouldUseDecapitalizedSimpleClassName() {
        // Arrange
        final String indexFileEntry = TestService.class.getName();

        // Act
        final Map<String, BeanDefinition> beanDefinitions =
                generateOsgiServiceImportBeans(SERVICE_CLASS_LOADER, singleton(indexFileEntry));

        // Assert
        assertThat(beanDefinitions.size(), is(1));
        final String expectedBeanName = "testService"; // simple class name, de-capitalized
        assertThat(beanDefinitions, hasKey(expectedBeanName));
    }

    @Test
    public void generateOsgiServiceImportBeans_whenBeanNameGiven_shouldUseThatName() {
        // Arrange
        final String beanName = "myBeanName";
        final String indexFileEntry = TestService.class.getName() + "#" + beanName;

        // Act
        final Map<String, BeanDefinition> beanDefinitions =
                generateOsgiServiceImportBeans(SERVICE_CLASS_LOADER, singleton(indexFileEntry));

        // Assert
        assertThat(beanDefinitions.size(), is(1));
        assertThat(beanDefinitions, hasKey(beanName));
    }

    @Test
    public void generateOsgiServiceImportBeans_whenServicesHaveSameSimpleName_shouldGenerateBeanForEach() {
        // Arrange
        final String indexEntry1 = TestService.class.getName();
        final String indexEntry2 = com.atlassian.plugin.spring.scanner.runtime.impl.util.TestService.class.getName();

        // Act
        final Map<String, BeanDefinition> beanDefinitions =
                generateOsgiServiceImportBeans(SERVICE_CLASS_LOADER, asList(indexEntry1, indexEntry2));

        // Assert
        assertThat("Actual map: " + beanDefinitions, beanDefinitions.size(), is(2));
    }

    @Test
    public void generateOsgiServiceImportBeans_whenServicesHaveDifferentSimpleNames_shouldGenerateBeanForEach() {
        // Arrange
        final String indexEntry1 = TestService.class.getName();
        final String indexEntry2 = TestService2.class.getName();

        // Act
        final Map<String, BeanDefinition> beanDefinitions =
                generateOsgiServiceImportBeans(SERVICE_CLASS_LOADER, asList(indexEntry1, indexEntry2));

        // Assert
        assertThat("Actual map: " + beanDefinitions, beanDefinitions.size(), is(2));
        assertThat(beanDefinitions.keySet(), containsInAnyOrder("testService", "testService2"));
    }

    @Test
    public void generateOsgiServiceImportBeans_whenServicesHaveSameSimpleNameButOverridden_shouldGenerateBeanForEach() {
        // Arrange
        final String indexEntry1 = TestService.class.getName();
        final String secondBeanName = "work-around-SCANNER-2168";
        final String indexEntry2 = indexEntry1 + "#" + secondBeanName;

        // Act
        final Map<String, BeanDefinition> beanDefinitions =
                generateOsgiServiceImportBeans(SERVICE_CLASS_LOADER, asList(indexEntry1, indexEntry2));

        // Assert
        assertThat("Actual map: " + beanDefinitions, beanDefinitions.size(), is(2));
        assertThat(beanDefinitions.keySet(), containsInAnyOrder("testService", secondBeanName));
    }

    @Test
    public void generateOsgiServiceImportBeans_whenServiceClassIsNotLoadable_shouldNotGenerateBeanForIt() {
        // Arrange
        final String indexEntry = "com.example.services.FictitiousService";

        // Act
        final Map<String, BeanDefinition> beanDefinitions =
                generateOsgiServiceImportBeans(SERVICE_CLASS_LOADER, singleton(indexEntry));

        // Assert
        assertThat(beanDefinitions, is(emptyMap()));
    }

    /**
     * Dummy interface for use in tests; deliberately has the same simple name as an
     * interface in the {@code util} child package.
     */
    private interface TestService {}

    /**
     * Dummy interface for use in tests.
     */
    private interface TestService2 {}
}
