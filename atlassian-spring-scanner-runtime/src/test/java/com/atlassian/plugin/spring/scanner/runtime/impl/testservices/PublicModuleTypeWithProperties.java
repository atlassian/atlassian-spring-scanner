package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.annotation.export.ServiceProperty;

@ExportAsService(
        value = {ModuleType.class},
        properties = {
            @ServiceProperty(key = "module_key_1", value = "module_value_1"),
            @ServiceProperty(key = "module_key_2", value = "module_value_2")
        })
@Component
public class PublicModuleTypeWithProperties implements ExampleModule {}
