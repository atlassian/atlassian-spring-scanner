package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import org.springframework.stereotype.Component;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

@Aspect
@Component
public class ProxyEnforcerAspect {

    @Around("execution(public * com.atlassian.plugin.spring.scanner.runtime.impl.testservices.*Proxy.*(..))")
    public Object around(ProceedingJoinPoint method) throws Throwable {
        return method.proceed();
    }
}
