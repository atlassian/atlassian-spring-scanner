package com.atlassian.plugin.spring.scanner.runtime.impl;

import org.springframework.aop.support.AopUtils;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

class IsAopProxy extends BaseMatcher<Object> {
    @Override
    public void describeTo(Description description) {
        description.appendText("is AOP proxy");
    }

    @Override
    public boolean matches(Object item) {
        return AopUtils.isAopProxy(item);
    }

    public static Matcher<Object> aopProxy() {
        return new IsAopProxy();
    }
}
