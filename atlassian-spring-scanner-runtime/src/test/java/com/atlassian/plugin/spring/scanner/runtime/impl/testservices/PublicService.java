package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

@ExportAsService
@Component
public class PublicService implements Service {
    @Override
    public void a() {}
}
