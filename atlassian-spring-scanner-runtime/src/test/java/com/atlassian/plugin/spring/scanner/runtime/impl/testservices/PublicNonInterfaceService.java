package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

@Component
@ExportAsService
public class PublicNonInterfaceService {
    public void a() {}
}
