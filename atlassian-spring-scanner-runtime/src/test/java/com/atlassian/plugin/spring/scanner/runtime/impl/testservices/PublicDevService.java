package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;

@ExportAsDevService
@Component
public class PublicDevService implements Service {
    @Override
    public void a() {}
}
