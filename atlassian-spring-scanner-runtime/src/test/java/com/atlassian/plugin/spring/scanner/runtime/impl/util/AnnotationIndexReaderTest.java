package com.atlassian.plugin.spring.scanner.runtime.impl.util;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Properties;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.emptyArray;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

public class AnnotationIndexReaderTest {

    private static final String INDEX_FILE_NAME = "theIndexFile";
    private static final String RESOURCE_FILE = "theResourceFile";

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Bundle bundle;

    @Mock
    private BundleContext bundleContext;

    @Before
    public void setUp() {
        when(bundleContext.getBundle()).thenReturn(bundle);
    }

    private void setUpResourceFile(final URL desiredUrl) {
        when(bundle.getEntry(RESOURCE_FILE)).thenReturn(desiredUrl);
    }

    @Test
    public void readIndexFile_whenBundleHasNoSuchEntry_shouldReturnEmptyList() {
        // Arrange
        setUpResourceFile(null);

        // Act
        final List<String> entries = AnnotationIndexReader.readIndexFile(RESOURCE_FILE, bundle);

        // Assert
        assertThat(entries, is(emptyList()));
    }

    @Test
    public void readIndexFile_whenUrlIsNull_shouldReturnEmptyList() {
        assertThat(AnnotationIndexReader.readIndexFile(null), is(emptyList()));
    }

    @Test
    public void readIndexFile_whenUrlIsInvalid_shouldReturnEmptyList() throws Exception {
        // Arrange
        final File nonExistentFile = new File("no-such-file.txt");

        // Act
        final List<String> entries =
                AnnotationIndexReader.readIndexFile(nonExistentFile.toURI().toURL());

        // Assert
        assertThat(entries, is(emptyList()));
    }

    @Test
    public void readIndexFile_whenUrlIsValid_shouldReturnFileContents() {
        // Arrange
        final URL fileUrl = getUrlOfFileInThisPackage("entries.txt");

        // Act
        final List<String> entries = AnnotationIndexReader.readIndexFile(fileUrl);

        // Assert
        assertThat(entries, contains("line 1", "line 2"));
    }

    @NotNull
    private URL getUrlOfFileInThisPackage(final String filename) {
        return requireNonNull(getClass().getResource(filename));
    }

    @Test
    public void readPropertiesFile_whenUrlIsNull_shouldReturnEmptyProperties() {
        // Act
        final Properties properties = AnnotationIndexReader.readPropertiesFile(null);

        // Assert
        assertNotNull(properties);
        assertTrue(properties.isEmpty());
    }

    @Test
    public void readPropertiesFile_whenUrlIsInvalid_shouldReturnEmptyProperties() throws Exception {
        // Arrange
        final File nonExistentFile = new File("no-such-file.txt");

        // Act
        final Properties properties =
                AnnotationIndexReader.readPropertiesFile(nonExistentFile.toURI().toURL());

        // Assert
        assertNotNull(properties);
        assertTrue(properties.isEmpty());
    }

    @Test
    public void readPropertiesFile_whenUrlIsValid_shouldReadItsProperties() {
        // Arrange
        final URL propertiesFileUrl = getUrlOfFileInThisPackage("test.properties");

        // Act
        final Properties properties = AnnotationIndexReader.readPropertiesFile(propertiesFileUrl);

        // Assert
        assertThat(properties.size(), is(2));
        assertThat(properties, hasEntry("foo", "bar"));
        assertThat(properties, hasEntry("baz", "bat"));
    }

    @Test
    public void splitProfiles_whenGivenNullString_shouldReturnEmptyArray() {
        assertThat(AnnotationIndexReader.splitProfiles(null), is(emptyArray()));
    }

    @Test
    public void splitProfiles_whenGivenEmptyString_shouldReturnEmptyArray() {
        assertThat(AnnotationIndexReader.splitProfiles(""), is(emptyArray()));
    }

    @Test
    public void splitProfiles_whenGivenStringWithNoDelimiter_shouldReturnArrayOfOneProfile() {
        assertThat(AnnotationIndexReader.splitProfiles("foo"), is(arrayContaining("foo")));
    }

    @Test
    public void splitProfiles_whenGivenStringWithOneDelimiter_shouldReturnArrayOfTwoProfiles() {
        assertThat(AnnotationIndexReader.splitProfiles("foo,bar"), is(arrayContaining("foo", "bar")));
    }

    @Test
    public void splitProfiles_whenGivenStringWithOTwoDelimiters_shouldReturnArrayOfThreeProfiles() {
        assertThat(AnnotationIndexReader.splitProfiles("foo,bar,baz"), is(arrayContaining("foo", "bar", "baz")));
    }

    @Test
    public void getIndexFilesForProfiles_whenGivenNoProfileNames_shouldReturnFilenameWithNoProfile() {
        assertIndexFilesForProfiles(new String[0], "META-INF/plugin-components/" + INDEX_FILE_NAME);
    }

    @Test
    public void getIndexFilesForProfiles_whenGivenOneProfileName_shouldReturnFilenameWithThatProfile() {
        final String profile = "theProfile";
        final String[] profileNames = {profile};
        assertIndexFilesForProfiles(
                profileNames, "META-INF/plugin-components/profile-" + profile + "/" + INDEX_FILE_NAME);
    }

    @Test
    public void getIndexFilesForProfiles_whenGivenTwoProfileNames_shouldReturnFilenamesWithThoseProfiles() {
        final String profile1 = "profile1";
        final String profile2 = "profile2";
        final String[] profileNames = {profile1, "", profile2};
        assertIndexFilesForProfiles(
                profileNames,
                "META-INF/plugin-components/profile-" + profile1 + "/" + INDEX_FILE_NAME,
                "META-INF/plugin-components/profile-" + profile2 + "/" + INDEX_FILE_NAME);
    }

    private static void assertIndexFilesForProfiles(final String[] profileNames, final String... expectedIndexFiles) {
        // Act
        final List<String> indexFiles = AnnotationIndexReader.getIndexFilesForProfiles(profileNames, INDEX_FILE_NAME);

        // Assert
        assertThat(indexFiles, contains(expectedIndexFiles));
    }

    @Test
    public void readAllIndexFilesForProduct_whenNoProductFilter_shouldReturnOnlyEntriesInGivenFile() {
        // Arrange
        final String resourceFile = "theResourceFile";
        final URL resourceFileUrl = getUrlOfFileInThisPackage("index-entries.txt");
        setUpResourceFile(resourceFileUrl);

        // Act
        final List<String> entries = AnnotationIndexReader.readAllIndexFilesForProduct(resourceFile, bundleContext);

        // Assert
        assertThat(entries, contains("entry1", "entry2"));
    }
}
