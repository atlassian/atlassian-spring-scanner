package com.atlassian.plugin.spring.scanner.runtime.impl.util;

/**
 * Dummy interface for use in tests; deliberately has the same simple name as an
 * interface inside {@code ComponentImportBeanFactoryPostProcessorTest}.
 */
public interface TestService {}
