package com.atlassian.plugin.spring.scanner.runtime.impl;

import java.util.Map;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PrivateService;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicDevService;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicDevServiceProxy;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicDevServiceWithProperties;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicDevServiceWithProperty;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicModuleTypeWithProperties;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicModuleTypeWithProperty;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicNonInterfaceService;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicNonInterfaceServiceProxy;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicService;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicServiceProxy;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicServiceWithProperties;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.PublicServiceWithProperty;
import com.atlassian.plugin.spring.scanner.runtime.impl.testservices.Service;

import static java.lang.Boolean.TRUE;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyMap;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.spring.scanner.runtime.impl.IsAopProxy.aopProxy;
import static com.atlassian.plugin.spring.scanner.runtime.impl.ServiceExporterBeanPostProcessor.ATLASSIAN_DEV_MODE_PROP;

public class ServiceExporterBeanPostProcessorTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Bundle bundle;

    @Mock
    private BundleContext bundleContext;

    @Mock
    private ConfigurableListableBeanFactory beanFactory;

    @Mock
    private ExportedServiceManager exportedServiceManager;

    @Captor
    private ArgumentCaptor<Map<String, Object>> propertiesCaptor;

    @InjectMocks
    private ServiceExporterBeanPostProcessor exporterPostProcessor;

    private ApplicationContext applicationContext;

    @Before
    public void setUp() {
        when(bundleContext.getBundle()).thenReturn(bundle);
        System.setProperty(ATLASSIAN_DEV_MODE_PROP, TRUE.toString());
        this.exporterPostProcessor.afterPropertiesSet();
        this.applicationContext = new ClassPathXmlApplicationContext(
                "classpath:/com/atlassian/plugin/spring/scanner/runtime/impl/testservices/testservicesContext.xml");
    }

    @Test
    public void testNonProxyExportAsService() throws Exception {
        testNonProxyBeanIsExported(PublicService.class, Service.class);
    }

    @Test
    public void testNonProxyExportAsDevService() throws Exception {
        testNonProxyBeanIsExported(PublicDevService.class, Service.class);
    }

    @Test
    public void testNonProxyNonInterfaceExportAsService() throws Exception {
        testNonProxyBeanIsExported(PublicNonInterfaceService.class, PublicNonInterfaceService.class);
    }

    @Test
    public void testProxyExportAsService() throws Exception {
        testProxyBeanIsExported(PublicServiceProxy.class, Service.class);
    }

    @Test
    public void testProxyExportAsDevService() throws Exception {
        testProxyBeanIsExported(PublicDevServiceProxy.class, Service.class);
    }

    @Test
    public void testProxyNonInterfaceExportAsService() throws Exception {
        testProxyBeanIsExported(PublicNonInterfaceServiceProxy.class, PublicNonInterfaceServiceProxy.class);
    }

    @Test
    public void testPrivateServiceNotExported() throws Exception {
        Map.Entry<String, ?> nameBeanEntry = getBeanByImplClass(PrivateService.class);
        assertThat(nameBeanEntry.getValue(), Matchers.any(Object.class));
        exporterPostProcessor.postProcessAfterInitialization(nameBeanEntry.getValue(), nameBeanEntry.getKey());

        verify(exportedServiceManager, never())
                .registerService(
                        any(BundleContext.class),
                        eq(nameBeanEntry.getValue()),
                        eq(nameBeanEntry.getKey()),
                        anyMap(),
                        any(Class.class));
    }

    @Test
    public void testPublicServiceExportedWithServiceProperty() throws Exception {
        testBeanIsExportedWithServiceProperty(
                PublicServiceWithProperty.class, "service_key", "service_with_properties_key");
    }

    @Test
    public void testPublicModuleTypeExportedWithServiceProperty() throws Exception {
        testBeanIsExportedWithServiceProperty(
                PublicModuleTypeWithProperty.class, "module_key", "module_type_with_properties_key");
    }

    @Test
    public void testPublicDevServiceExportedWithServiceProperty() throws Exception {
        testBeanIsExportedWithServiceProperty(
                PublicDevServiceWithProperty.class, "dev_key", "dev_service_with_properties_key");
    }

    @Test
    public void testPublicServiceExportedWithServiceProperties() throws Exception {
        Map<String, Object> properties = getPropertiesFromExportedService(PublicServiceWithProperties.class);

        assertThat(properties.size(), equalTo(2));
        assertThat(properties.get("service_key_1"), equalTo("service_value_1"));
        assertThat(properties.get("service_key_2"), equalTo("service_value_2"));
    }

    @Test
    public void testPublicDevServiceExportedWithServiceProperties() throws Exception {
        Map<String, Object> properties = getPropertiesFromExportedService(PublicDevServiceWithProperties.class);

        assertThat(properties.size(), equalTo(2));
        assertThat(properties.get("dev_key_1"), equalTo("dev_value_1"));
        assertThat(properties.get("dev_key_2"), equalTo("dev_value_2"));
    }

    @Test
    public void testPublicModuleTypeExportedWithServiceProperties() throws Exception {
        Map<String, Object> properties = getPropertiesFromExportedService(PublicModuleTypeWithProperties.class);

        assertThat(properties.size(), equalTo(2));
        assertThat(properties.get("module_key_1"), equalTo("module_value_1"));
        assertThat(properties.get("module_key_2"), equalTo("module_value_2"));
    }

    private Map<String, Object> getPropertiesFromExportedService(final Class<?> serviceClass) throws Exception {
        Map.Entry<String, ?> nameBeanEntry = getBeanByImplClass(serviceClass);
        assertThat(nameBeanEntry.getValue(), CoreMatchers.any(Object.class));

        exporterPostProcessor.postProcessAfterInitialization(nameBeanEntry.getValue(), nameBeanEntry.getKey());

        verify(exportedServiceManager)
                .registerService(
                        any(BundleContext.class),
                        eq(nameBeanEntry.getValue()),
                        eq(nameBeanEntry.getKey()),
                        propertiesCaptor.capture(),
                        any(Class.class));

        return propertiesCaptor.getValue();
    }

    private void testBeanIsExportedWithServiceProperty(Class<?> serviceClass, String key, String expectedProperty)
            throws Exception {
        Map<String, Object> properties = getPropertiesFromExportedService(serviceClass);
        assertThat(properties.size(), equalTo(1));
        assertThat(properties.get(key), equalTo(expectedProperty));
    }

    private void testBeanIsExported(
            final Class<?> beanImplClass, final Matcher<Object> beanMatcher, final Class<?> expectedInterface)
            throws Exception {
        final Map.Entry<String, ?> nameBeanEntry = getBeanByImplClass(beanImplClass);
        assertThat(nameBeanEntry.getValue(), beanMatcher);
        exporterPostProcessor.postProcessAfterInitialization(nameBeanEntry.getValue(), nameBeanEntry.getKey());

        verify(exportedServiceManager)
                .registerService(
                        any(BundleContext.class),
                        eq(nameBeanEntry.getValue()),
                        eq(nameBeanEntry.getKey()),
                        anyMap(),
                        eq(expectedInterface));
    }

    private void testNonProxyBeanIsExported(Class<?> beanImplClass, Class<?> expectInstanceOf) throws Exception {
        testBeanIsExported(beanImplClass, not(aopProxy()), expectInstanceOf);
    }

    private void testProxyBeanIsExported(Class<?> beanImplClass, Class<?> expectInstanceOf) throws Exception {
        testBeanIsExported(beanImplClass, aopProxy(), expectInstanceOf);
    }

    protected Map.Entry<String, Object> getBeanByImplClass(Class<?> beanImplClass) {
        final String beanName = getBeanDefaultName(beanImplClass);
        final Object bean = applicationContext.getBean(beanName);
        assertThat(bean, notNullValue());
        return new ImmutableMapEntry<>(beanName, bean);
    }

    private static class ImmutableMapEntry<K, V> implements Map.Entry<K, V> {

        private final K key;
        private final V value;

        private ImmutableMapEntry(final K key, final V value) {
            this.key = requireNonNull(key);
            this.value = value;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return value;
        }

        @Override
        public V setValue(V value) {
            throw new UnsupportedOperationException();
        }
    }

    private String getBeanDefaultName(Class<?> type) {
        String name = type.getSimpleName();
        return name.substring(0, 1).toLowerCase() + name.substring(1);
    }
}
