package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.annotation.export.ServiceProperty;

@ModuleType(
        value = {Service.class},
        properties = {@ServiceProperty(key = "module_key", value = "module_type_with_properties_key")})
@Component
public class PublicModuleTypeWithProperty implements ExampleModule {}
