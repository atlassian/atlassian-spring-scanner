package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.export.ServiceProperty;

@ExportAsService(
        value = {Service.class},
        properties = {
            @ServiceProperty(key = "service_key_1", value = "service_value_1"),
            @ServiceProperty(key = "service_key_2", value = "service_value_2")
        })
@Component
public class PublicServiceWithProperties implements Service {
    @Override
    public void a() {}
}
