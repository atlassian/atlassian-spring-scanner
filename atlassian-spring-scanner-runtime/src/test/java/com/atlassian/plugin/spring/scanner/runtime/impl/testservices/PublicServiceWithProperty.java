package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.export.ServiceProperty;

@ExportAsService(
        value = {Service.class},
        properties = {@ServiceProperty(key = "service_key", value = "service_with_properties_key")})
@Component
public class PublicServiceWithProperty implements Service {
    @Override
    public void a() {}
}
