package com.atlassian.plugin.spring.scanner.core;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Named;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.BitbucketComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.ConfluenceComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.CrowdComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.FecruComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.JiraComponent;
import com.atlassian.plugin.spring.scanner.annotation.component.RefappComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.BitbucketImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ConfluenceImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.CrowdImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.FecruImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import com.atlassian.plugin.spring.scanner.annotation.imports.RefappImport;
import com.atlassian.plugin.spring.scanner.core.vfs.VirtualFile;
import com.atlassian.plugin.spring.scanner.core.vfs.VirtualFileFactory;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Collections.unmodifiableMap;
import static java.util.Objects.requireNonNull;

import static com.atlassian.plugin.spring.scanner.ProductFilter.BAMBOO;
import static com.atlassian.plugin.spring.scanner.ProductFilter.BITBUCKET;
import static com.atlassian.plugin.spring.scanner.ProductFilter.CONFLUENCE;
import static com.atlassian.plugin.spring.scanner.ProductFilter.CROWD;
import static com.atlassian.plugin.spring.scanner.ProductFilter.FECRU;
import static com.atlassian.plugin.spring.scanner.ProductFilter.JIRA;
import static com.atlassian.plugin.spring.scanner.ProductFilter.REFAPP;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_DEV_EXPORT_KEY;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_EXPORT_KEY;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_IMPORT_KEY;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_KEY;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.COMPONENT_PRIMARY_KEY;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.DEFAULT_PROFILE_NAME;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.INDEX_FILES_DIR;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.PROFILE_PREFIX;

/**
 * Writes the component and import index files into their specific directories and specific index file names.
 * <p>
 * The Reflections / Javassist code deals only in strings, hence this is written as such.
 */
public class SpringIndexWriter {

    protected static final List<String> KNOWN_PRODUCT_IMPORT_ANNOTATIONS = asList(
            BambooImport.class.getCanonicalName(),
            BitbucketImport.class.getCanonicalName(),
            ConfluenceImport.class.getCanonicalName(),
            FecruImport.class.getCanonicalName(),
            JiraImport.class.getCanonicalName(),
            RefappImport.class.getCanonicalName(),
            CrowdImport.class.getCanonicalName());

    private final Map<String, RecordedAnnotationsHolder> recordedProfiles = new HashMap<>();
    private final VirtualFileFactory fileFactory;

    /**
     * A writer that can record and write component index files.
     *
     * @param baseDir the base directory to write to. Typically this is the class file output directory
     */
    public SpringIndexWriter(final String baseDir) {
        // since we see EVERYTHING in the byte code scanner, we can always be clean
        // and start from scratch.
        cleanDirectory(new File(baseDir, INDEX_FILES_DIR));
        fileFactory = baseDir != null ? new VirtualFileFactory(new File(baseDir)) : null;
    }

    /**
     * Indicates whether the given annotation is of interest to Spring Scanner.
     *
     * @param annotationType the fully-qualified class name of an annotation type
     * @return {@code false} if {@code null} is given
     */
    public boolean isInteresting(@Nullable final String annotationType) {
        return MeaningfulAnnotation.fromCanonicalName(annotationType).isPresent();
    }

    /**
     * Indicates whether the given annotation is both "interesting" and applicable to a method parameter.
     *
     * @param annotationType the fully-qualified class name of an annotation type
     * @return {@code false} if {@code null} is given
     * @see #isInteresting(String)
     */
    public boolean isParameterAnnotation(@Nullable final String annotationType) {
        return MeaningfulAnnotation.fromCanonicalName(annotationType)
                .map(a -> a.parameterAnnotation)
                .orElse(false);
    }

    /**
     * Indicates whether the given annotation is both "interesting" and applicable to a field.
     *
     * @param annotationType the fully-qualified class name of an annotation type
     * @return {@code false} if {@code null} is given
     * @see #isInteresting(String)
     */
    public boolean isFieldAnnotation(@Nullable final String annotationType) {
        return MeaningfulAnnotation.fromCanonicalName(annotationType)
                .map(a -> a.fieldAnnotation)
                .orElse(false);
    }

    /**
     * Informs this object that the given annotation was encountered while scanning the user's project.
     *
     * @param targetProfiles if empty, the {@code default} profile will be used
     * @param annotationType the fully-qualified class name of the annotation that was encountered
     * @param nameFromAnnotation the bean name provided by the annotation, if any
     * @param componentClass the fully-qualified name of the annotated component's class
     */
    public void encounteredAnnotation(
            final Set<String> targetProfiles,
            final String annotationType,
            @Nullable final String nameFromAnnotation,
            final String componentClass) {
        final Collection<String> profiles = new HashSet<>(targetProfiles);
        if (profiles.isEmpty()) {
            // when we have no profiles, we go into the magic one called 'default'
            profiles.add(DEFAULT_PROFILE_NAME);
        }
        for (final String profile : profiles) {
            recordedProfiles
                    .computeIfAbsent(profile, ignored -> new RecordedAnnotationsHolder())
                    .recordAnnotation(annotationType, nameFromAnnotation, componentClass);
        }
    }

    /**
     * Writes the index files based on the annotations encountered so far.
     *
     * @see #encounteredAnnotation(Set, String, String, String)
     */
    public void writeIndexes() {
        for (Map.Entry<String, RecordedAnnotationsHolder> annotationsEntry : recordedProfiles.entrySet()) {
            writeProfileIndexes(annotationsEntry.getKey(), annotationsEntry.getValue());
        }
    }

    private void writeProfileIndexes(final String profileName, final RecordedAnnotationsHolder annotations) {
        // the javac annotation processing only allows you to open a file for input and output ONCE
        // for reasons you can look up online. Basically so it knows what it has seen before.
        // So if you open a file, write to it and then try to repeat that, it will blow up.
        //
        // So we need to collapse the map here, so that we go from many annotations sharing the
        // same index file to a map of index file -> all components for that index file
        // and then write it out per unique file so they are only opened once.
        final Map<String, Set<String>> fileNameToComponents = collapse(annotations);
        // now that we have them in file name order, we can write them safely under the javac world
        for (Map.Entry<String, Set<String>> entry : fileNameToComponents.entrySet()) {
            try {
                writeIndexFile(profileName, entry.getKey(), entry.getValue());
            } catch (IOException e) {
                throw new RuntimeIOException(e);
            }
        }
    }

    private Map<String, Set<String>> collapse(final RecordedAnnotationsHolder annotations) {
        final Map<String, Set<String>> fileNameToComponents = new HashMap<>();
        for (Map.Entry<MeaningfulAnnotation, Set<String>> entry :
                annotations.getRecordedAnnotations().entrySet()) {
            MeaningfulAnnotation meaningfulAnnotation = entry.getKey();
            if (meaningfulAnnotation.isWrittenToDisk()) {
                final Set<String> perAnnotationComponents = entry.getValue();
                final String indexFileName = meaningfulAnnotation.getFileName();
                fileNameToComponents
                        .computeIfAbsent(indexFileName, key -> new TreeSet<>())
                        .addAll(perAnnotationComponents);
            }
        }
        return fileNameToComponents;
    }

    private void writeIndexFile(final String profileName, final String indexFileName, final Set<String> entries)
            throws IOException {
        if (fileFactory == null) {
            return;
        }

        final File file = makeProfiledFileName(profileName, indexFileName);

        final VirtualFile vf = fileFactory.getFile(file.getPath());

        final Set<String> lines = new TreeSet<>();
        // read the existing lines
        lines.addAll(vf.readLines());
        lines.addAll(entries);
        vf.writeLines(lines);
    }

    private File makeProfiledFileName(final String profileName, final String fileName) {
        final File file;
        if (DEFAULT_PROFILE_NAME.equals(profileName)) {
            // the well known default profile goes into the top level directory
            // and only explicitly defined profiles go into sub directories
            file = new File(fileName);
        } else {
            file = new File(PROFILE_PREFIX + profileName, fileName);
        }
        return new File(INDEX_FILES_DIR, file.getPath());
    }

    private void cleanDirectory(final File destination) {
        if (destination.exists()) {
            try {
                Files.walkFileTree(destination.toPath(), new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
                        if (e != null) {
                            throw e;
                        }
                        Files.delete(dir);
                        return FileVisitResult.CONTINUE;
                    }

                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        Files.delete(file);
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e) {
                throw new RuntimeIOException("Unable to delete directory " + destination, e);
            }
        }
    }

    // Visible for testing
    enum MeaningfulAnnotation {
        // Well-known Spring annotations
        COMPONENT(COMPONENT_KEY, Component.class),
        SERVICE(COMPONENT_KEY, Service.class),
        CONTROLLER(COMPONENT_KEY, Controller.class),
        REPOSITORY(COMPONENT_KEY, Repository.class),
        PRIMARY(COMPONENT_PRIMARY_KEY, Primary.class),

        // JSR annotations
        SINGLETON(COMPONENT_KEY, javax.inject.Singleton.class, true, true),
        NAMED(COMPONENT_KEY, Named.class, false, true),

        // Our component annotations
        CLASSPATH_COMPONENT(COMPONENT_KEY, ClasspathComponent.class, true, true),
        BAMBOO_COMPONENT(BAMBOO.getPerProductFile(COMPONENT_KEY), BambooComponent.class, true, true),
        BITBUCKET_COMPONENT(BITBUCKET.getPerProductFile(COMPONENT_KEY), BitbucketComponent.class, true, true),
        CONFLUENCE_COMPONENT(CONFLUENCE.getPerProductFile(COMPONENT_KEY), ConfluenceComponent.class, true, true),
        JIRA_COMPONENT(JIRA.getPerProductFile(COMPONENT_KEY), JiraComponent.class, true, true),
        FECRU_COMPONENT(FECRU.getPerProductFile(COMPONENT_KEY), FecruComponent.class, true, true),
        REFAPP_COMPONENT(REFAPP.getPerProductFile(COMPONENT_KEY), RefappComponent.class, true, true),
        CROWD_COMPONENT(CROWD.getPerProductFile(COMPONENT_KEY), CrowdComponent.class, true, true),

        // Our import annotations
        IMPORTS(COMPONENT_IMPORT_KEY, ComponentImport.class, true, true),
        BAMBOO_IMPORTS(BAMBOO.getPerProductFile(COMPONENT_IMPORT_KEY), BambooImport.class, true, true),
        BITBUCKET_IMPORTS(BITBUCKET.getPerProductFile(COMPONENT_IMPORT_KEY), BitbucketImport.class, true, true),
        CONFLUENCE_IMPORTS(CONFLUENCE.getPerProductFile(COMPONENT_IMPORT_KEY), ConfluenceImport.class, true, true),
        FECRU_IMPORTS(FECRU.getPerProductFile(COMPONENT_IMPORT_KEY), FecruImport.class, true, true),
        JIRA_IMPORTS(JIRA.getPerProductFile(COMPONENT_IMPORT_KEY), JiraImport.class, true, true),
        REFAPP_IMPORTS(REFAPP.getPerProductFile(COMPONENT_IMPORT_KEY), RefappImport.class, true, true),
        CROWD_IMPORTS(CROWD.getPerProductFile(COMPONENT_IMPORT_KEY), CrowdImport.class, true, true),

        // Our export annotations
        EXPORT_AS_SERVICE(COMPONENT_EXPORT_KEY, ExportAsService.class, true, true),
        EXPORT_AS_DEV_SERVICE(COMPONENT_DEV_EXPORT_KEY, ExportAsDevService.class, true, true),
        MODULE_TYPE(COMPONENT_EXPORT_KEY, ModuleType.class);

        private static final Map<String, MeaningfulAnnotation> CANONICAL_NAME_INDEX = new HashMap<>();

        static {
            // Populate the map of annotations keyed by their canonical class names
            stream(values())
                    .forEach(meaningfulAnnotation -> CANONICAL_NAME_INDEX.put(
                            meaningfulAnnotation.forAnnotation.getCanonicalName(), meaningfulAnnotation));
        }

        @Nonnull
        private static Optional<MeaningfulAnnotation> fromCanonicalName(final String annotationType) {
            return Optional.ofNullable(CANONICAL_NAME_INDEX.get(annotationType));
        }

        private final String fileName;
        private final Class<?> forAnnotation;
        private final boolean parameterAnnotation;
        private final boolean fieldAnnotation;

        MeaningfulAnnotation(final String fileName, final Class<?> forAnnotation) {
            this(fileName, forAnnotation, false, false);
        }

        MeaningfulAnnotation(
                final String fileName,
                final Class<?> forAnnotation,
                final boolean parameterAnnotation,
                final boolean fieldAnnotation) {
            this.fileName = requireNonNull(fileName);
            this.forAnnotation = requireNonNull(forAnnotation);
            this.parameterAnnotation = parameterAnnotation;
            this.fieldAnnotation = fieldAnnotation;
        }

        private String getFileName() {
            return fileName;
        }

        private boolean isWrittenToDisk() {
            return fileName != null;
        }
    }

    /**
     * Simple holder class to record sets of annotations as we traverse the classes.
     */
    static class RecordedAnnotationsHolder {

        final Map<MeaningfulAnnotation, Set<String>> recordedAnnotations = new EnumMap<>(MeaningfulAnnotation.class);

        /**
         * Returns a map of the recorded annotations to the components they annotate.
         *
         * @return an unmodifiable copy of this map
         */
        @Nonnull
        public Map<MeaningfulAnnotation, Set<String>> getRecordedAnnotations() {
            return unmodifiableMap(recordedAnnotations);
        }

        /**
         * Records a usage of the given annotation on the given component.
         *
         * @param annotationType the fully-qualified name of the annotation type
         * @param nameFromAnnotation the component name provided by the annotation, if any
         * @param componentClass the fully-qualified name of the component type
         * @throws IllegalStateException if the given annotation is not "interesting"
         * @see #isInteresting(String)
         */
        public void recordAnnotation(
                final String annotationType, @Nullable final String nameFromAnnotation, final String componentClass) {
            final MeaningfulAnnotation annotation = MeaningfulAnnotation.fromCanonicalName(annotationType)
                    .orElseThrow(() -> new IllegalStateException("Annotation " + annotationType + " not found"));
            recordAnnotation(annotation, getIndexEntry(componentClass, nameFromAnnotation));
        }

        private static String getIndexEntry(final String componentClass, @Nullable final String nameFromAnnotation) {
            final StringBuilder entry = new StringBuilder(componentClass);
            if (nameFromAnnotation != null) {
                final String trimmed = nameFromAnnotation.trim();
                if (!trimmed.isEmpty()) {
                    entry.append("#").append(trimmed);
                }
            }
            return entry.toString();
        }

        private void recordAnnotation(final MeaningfulAnnotation meaningfulAnnotation, final String value) {
            recordedAnnotations
                    .computeIfAbsent(meaningfulAnnotation, key -> new TreeSet<>())
                    .add(value);
        }
    }

    /**
     * Allows us to throw IOExceptions as RuntimeExceptions without using that too-general class.
     */
    private static class RuntimeIOException extends RuntimeException {

        RuntimeIOException(final IOException cause) {
            super(cause);
        }

        public RuntimeIOException(final String message, final IOException cause) {
            super(message, cause);
        }
    }
}
