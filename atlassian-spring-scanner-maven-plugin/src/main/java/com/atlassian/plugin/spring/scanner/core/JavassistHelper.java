package com.atlassian.plugin.spring.scanner.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.ParameterAnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.ArrayMemberValue;
import javassist.bytecode.annotation.ClassMemberValue;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.StringMemberValue;
import javax.annotation.Nullable;

import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static javassist.bytecode.AnnotationsAttribute.visibleTag;

/**
 * A helper file of Javassist related code.
 *
 * The Reflections code gives us some help and an invocation framework,
 * however it doesn't quite give us enough, so we use Javassist.
 */
public class JavassistHelper {

    public List<String> getClassAnnotationNames(final ClassFile aClass) {
        return getAnnotationNames((AnnotationsAttribute) aClass.getAttribute(visibleTag));
    }

    private List<String> getAnnotationNames(final AnnotationsAttribute... annotationsAttributes) {
        if (annotationsAttributes == null) {
            return emptyList();
        }
        return stream(annotationsAttributes)
                .filter(Objects::nonNull)
                .flatMap(annotationsAttribute -> stream(annotationsAttribute.getAnnotations()))
                .map(Annotation::getTypeName)
                .collect(toList());
    }

    /**
     * Returns the value of the annotation as a string value
     *
     * @param classFile      the class to inspect
     * @param annotationType the name of the annotation type
     * @return a value or null if it has no member
     */
    Set<String> getAnnotationMemberSet(final ClassFile classFile, final String annotationType) {
        final MemberValue value = getMemberValue(classFile, annotationType);
        if (value instanceof ArrayMemberValue) {
            final Set<String> values = new HashSet<>();
            final ArrayMemberValue arrayMemberValue = (ArrayMemberValue) value;
            final MemberValue[] arrayMemberValueValues = arrayMemberValue.getValue();
            for (final MemberValue memberValue : arrayMemberValueValues) {
                if (memberValue != null) {
                    values.add(removeQuotes(memberValue.toString()));
                }
            }
            return values;
        }
        return emptySet();
    }

    private MemberValue getMemberValue(final ClassFile classFile, final String annotationType) {
        final AnnotationsAttribute annotations = (AnnotationsAttribute) classFile.getAttribute(visibleTag);
        final Annotation annotation = annotations.getAnnotation(annotationType);
        return annotation == null ? null : annotation.getMemberValue("value");
    }

    /*
     * Javassist gives us the annotations wrapped in the original quotes "
     */
    private String removeQuotes(String s) {
        if (s.startsWith("\"")) {
            s = s.substring(1);
        }
        if (s.endsWith("\"")) {
            s = s.substring(0, s.length() - 1);
        }
        return s;
    }

    /**
     * Returns a named "member" of the annotation as a string value
     *
     * @param classFile      the class to inspect
     * @param annotationType the name of the annotation type
     * @return a value or null if it has no member
     */
    String getAnnotationMember(final ClassFile classFile, final String annotationType) {
        final MemberValue value = getMemberValue(classFile, annotationType);
        return dataForMemberValue(value);
    }

    /**
     * Returns the {@code value} of the given annotation.
     *
     * @param annotation the annotation to inspect
     * @return the value or null if it has no value
     */
    String getAnnotationMember(@Nullable final Annotation annotation) {
        final MemberValue value = annotation == null ? null : annotation.getMemberValue("value");
        return dataForMemberValue(value);
    }

    private String dataForMemberValue(final MemberValue memberValue) {
        if (memberValue instanceof StringMemberValue) {
            return ((StringMemberValue) memberValue).getValue();
        } else if (memberValue instanceof ClassMemberValue) {
            return ((ClassMemberValue) memberValue).getValue();
        } else if (memberValue instanceof ArrayMemberValue) {
            final MemberValue[] entryValues = ((ArrayMemberValue) memberValue).getValue();
            return stream(entryValues).map(this::dataForMemberValue).collect(joining(","));
        } else {
            // This branch is entered if memberValue is null, which happens when the annoation value is being defaulted.
            // The non-null branch is preserving legacy behaviour, as far as i can see no non null values not handled by
            // previous
            // branches of the if can be generated by current code.
            return memberValue == null ? null : removeQuotes(memberValue.toString());
        }
    }

    /**
     * Returns the list of annotations a method parameter might have.
     *
     * @param method         the method to inspect
     * @param parameterIndex the index of the method parameter to look at
     * @return a list of parameter annotations it may have
     */
    List<Annotation> getParameterAnnotations(final MethodInfo method, final int parameterIndex) {
        final List<Annotation> result = new ArrayList<>();

        final ParameterAnnotationsAttribute parameterAnnotationsAttribute =
                (ParameterAnnotationsAttribute) method.getAttribute(ParameterAnnotationsAttribute.visibleTag);
        if (parameterAnnotationsAttribute != null) {
            final Annotation[][] allAnnotations = parameterAnnotationsAttribute.getAnnotations();
            if (parameterIndex < allAnnotations.length) {
                final Annotation[] annotations = allAnnotations[parameterIndex];
                if (annotations != null) {
                    Collections.addAll(result, annotations);
                }
            }
        }
        return result;
    }
}
