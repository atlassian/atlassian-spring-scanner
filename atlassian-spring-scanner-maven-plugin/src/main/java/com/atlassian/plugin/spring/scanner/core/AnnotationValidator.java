package com.atlassian.plugin.spring.scanner.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;
import javax.inject.Named;
import javax.inject.Singleton;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.component.ClasspathComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

import static com.atlassian.plugin.spring.scanner.core.SpringIndexWriter.KNOWN_PRODUCT_IMPORT_ANNOTATIONS;

/**
 * Validates annotations found by the scanner.
 *
 * @since 2.1.17 was in AtlassianSpringByteCodeScanner
 */
@NotThreadSafe
class AnnotationValidator {

    private final Set<String> declaredComponentAnnotationTypes;
    private final Set<String> importedComponentAnnotationTypes;

    private final AnnotationReferences declaredComponents;
    private final AnnotationReferences importedComponents;

    AnnotationValidator() {
        this.declaredComponents = new AnnotationReferences();

        // All annotation types that cause us to create a component
        this.declaredComponentAnnotationTypes = new HashSet<>();
        this.declaredComponentAnnotationTypes.add(ExportAsService.class.getCanonicalName());
        this.declaredComponentAnnotationTypes.add(Component.class.getCanonicalName());
        this.declaredComponentAnnotationTypes.add(Named.class.getCanonicalName());
        this.declaredComponentAnnotationTypes.add(Singleton.class.getCanonicalName());

        this.importedComponents = new AnnotationReferences();

        // All annotation types that cause us to import a component
        this.importedComponentAnnotationTypes = new HashSet<>();
        this.importedComponentAnnotationTypes.add(ComponentImport.class.getCanonicalName());
        this.importedComponentAnnotationTypes.add(ClasspathComponent.class.getCanonicalName());
        this.importedComponentAnnotationTypes.addAll(KNOWN_PRODUCT_IMPORT_ANNOTATIONS);
    }

    public void encounteredAnnotation(
            final String annotationType, final String component, final String whereReferenced) {
        if (importedComponentAnnotationTypes.contains(annotationType)) {
            importedComponents.addReference(component, whereReferenced);
        } else if (declaredComponentAnnotationTypes.contains(annotationType)) {
            declaredComponents.addReference(component, whereReferenced);
        }
    }

    /**
     * Validates the annotations encountered so far.
     *
     * @return any validation errors
     */
    @Nonnull
    public List<String> validate() {
        final Set<String> conflictingComponents = importedComponents.intersect(declaredComponents);
        return conflictingComponents.stream().map(this::getError).collect(toList());
    }

    private String getError(final String conflictingComponent) {
        final List<String> imports = importedComponents.getReferences(conflictingComponent);
        final List<String> declarations = declaredComponents.getReferences(conflictingComponent);
        return format(
                "Cannot import a component within a plugin that declares the same component. "
                        + "Component '%s' was declared in %s but imported in %s",
                conflictingComponent, Arrays.toString(declarations.toArray()), Arrays.toString(imports.toArray()));
    }

    /**
     * Mantains a collection of annotated component names, and for each annotation, the places it was found.
     */
    @NotThreadSafe
    static class AnnotationReferences {

        private final Map<String, List<String>> references;

        public AnnotationReferences() {
            this.references = new HashMap<>();
        }

        public void addReference(final String component, final String whereReferenced) {
            final List<String> sources = references.computeIfAbsent(component, k -> new ArrayList<>());
            sources.add(whereReferenced);
        }

        public List<String> getReferences(final String component) {
            return references.get(component);
        }

        public Set<String> intersect(final AnnotationReferences that) {
            final Set<String> intersection = new HashSet<>(this.references.keySet());
            intersection.retainAll(that.references.keySet());
            return intersection;
        }
    }
}
