package com.atlassian.plugin.spring.scanner.core;

import java.net.URL;
import java.util.Collection;
import java.util.Collections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The configuration of an {@link AtlassianSpringByteCodeScanner}.
 */
public class ByteCodeScannerConfiguration {

    private final Logger log;
    private final Collection<URL> classPathUrls;
    private final String includeExclude;
    private final String outputDirectory;
    private final boolean verbose;
    private final boolean permitDuplicateImports;

    ByteCodeScannerConfiguration(
            final Logger log,
            final Collection<URL> classPathUrls,
            final String includeExclude,
            final String outputDirectory,
            final boolean verbose,
            final boolean permitDuplicateImports) {
        this.log = log;
        this.classPathUrls = classPathUrls;
        this.includeExclude = includeExclude;
        this.outputDirectory = outputDirectory;
        this.verbose = verbose;
        this.permitDuplicateImports = permitDuplicateImports;
    }

    public Collection<URL> getClassPathUrls() {
        return classPathUrls;
    }

    public String getIncludeExclude() {
        return includeExclude;
    }

    public Logger getLog() {
        return log;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public boolean isVerbose() {
        return verbose;
    }

    public boolean shouldValidate() {
        return !permitDuplicateImports;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private Logger log;
        private Collection<URL> classPathUrls = Collections.emptySet();
        private String includeExclude = "";
        private String outputDirectory;
        private boolean verbose;
        private boolean permitDuplicateImports;

        Builder() {
            log = LoggerFactory.getLogger(AtlassianSpringByteCodeScanner.class.getName());
        }

        public Builder setLog(Logger log) {
            this.log = log;
            return this;
        }

        public Builder setIncludeExclude(final String includeExclude) {
            this.includeExclude = includeExclude;
            return this;
        }

        public Builder setClassPathUrls(final Collection<URL> classPathUrls) {
            this.classPathUrls = classPathUrls;
            return this;
        }

        public Builder setOutputDirectory(final String outputDirectory) {
            this.outputDirectory = outputDirectory;
            return this;
        }

        public Builder setVerbose(final boolean verbose) {
            this.verbose = verbose;
            return this;
        }

        public Builder setPermitDuplicateImports(final boolean permitDuplicateImports) {
            this.permitDuplicateImports = permitDuplicateImports;
            return this;
        }

        public ByteCodeScannerConfiguration build() {
            return new ByteCodeScannerConfiguration(
                    log, classPathUrls, includeExclude, outputDirectory, verbose, permitDuplicateImports);
        }
    }
}
