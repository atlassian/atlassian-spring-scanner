package com.atlassian.plugin.spring.scanner.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.Descriptor;
import javassist.bytecode.FieldInfo;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.annotation.Annotation;

import org.springframework.stereotype.Component;
import org.reflections.Store;
import org.reflections.scanners.AbstractScanner;
import org.slf4j.Logger;

import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static java.lang.String.format;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;
import static javassist.bytecode.AnnotationsAttribute.visibleTag;

import static com.atlassian.plugin.spring.scanner.core.SpringIndexWriter.KNOWN_PRODUCT_IMPORT_ANNOTATIONS;
import static com.atlassian.plugin.spring.scanner.util.CommonConstants.HOST_CONTAINER_CLASS;

/**
 * A {@link org.reflections.scanners.Scanner} that scans for well-known injection annotations such as {@link Component},
 * collecting the results in an {@link AnnotationValidator} and a {@link SpringIndexWriter}.
 *
 * @since 2.1.17 was in AtlassianSpringByteCodeScanner
 */
@SuppressWarnings("unchecked")
class ClassScanner extends AbstractScanner {

    private final boolean verbose;
    private final AnnotationValidator annotationValidator;
    private final JavassistHelper javassistHelper;
    private final List<String> errors = new ArrayList<>();
    private final Logger log;
    private final ProfileFinder profileFinder;
    private final SpringIndexWriter springIndexWriter;

    private int classesEncountered;
    private int componentClassesEncountered;

    ClassScanner(
            final AnnotationValidator annotationValidator,
            final JavassistHelper javassistHelper,
            final Logger log,
            final ProfileFinder profileFinder,
            final SpringIndexWriter springIndexWriter,
            final boolean verbose) {
        this.annotationValidator = requireNonNull(annotationValidator);
        this.javassistHelper = requireNonNull(javassistHelper);
        this.log = requireNonNull(log);
        this.profileFinder = requireNonNull(profileFinder);
        this.springIndexWriter = requireNonNull(springIndexWriter);
        this.verbose = verbose;
    }

    @Override
    public void scan(final Object classObject, final Store store) {
        final ClassFile classFile = (ClassFile) classObject;
        try {
            scanClass(classFile);
        } catch (final RuntimeException e) {
            log.error("Unable to run byte code scanner on class {}. Continuing to the next class...", classFile);
        }
    }

    private void scanClass(final ClassFile classFile) {
        classesEncountered++;
        final Set<String> profiles = profileFinder.getProfiles(classFile);
        final List<String> classAnnotations = getMetadataAdapter().getClassAnnotationNames(classFile);
        for (final String annotation : classAnnotations) {
            if (isInteresting(annotation)) {
                final String className = getMetadataAdapter().getClassName(classFile);
                if (!isInstantiableClass(classFile)) {
                    log("\t\t(X) Class not suitable '%s' for annotation '%s'", className, annotation);
                    return;
                }
                encounteredComponentClass(classFile, profiles, annotation, className);
            }
        }

        // find constructor parameter imports etc.
        scanConstructors(classFile, profiles);

        // find field annotated imports etc.
        scanFields(classFile, profiles);
    }

    private void encounteredComponentClass(
            final ClassFile classFile, final Set<String> profiles, final String annotation, final String className) {
        componentClassesEncountered++;
        final String nameFromAnnotation = javassistHelper.getAnnotationMember(classFile, annotation);
        log("\t(/) Found annotation '%s' inside class '%s' with name '%s'", annotation, className, nameFromAnnotation);
        springIndexWriter.encounteredAnnotation(profiles, annotation, nameFromAnnotation, className);
        annotationValidator.encounteredAnnotation(annotation, className, className);

        if (ModuleType.class.getCanonicalName().equals(annotation)) {
            // Using @ModuleType requires the plugin to have a HostContainer component, so we simulate the user
            // having declared a SpringHostContainer as a component, so that our runtime will instantiate it.
            // This avoids plugin developers having to declare that component themselves.
            springIndexWriter.encounteredAnnotation(
                    profiles, Component.class.getCanonicalName(), "", HOST_CONTAINER_CLASS);
        }
    }

    private void log(final String messageFormat, final Object... args) {
        if (log.isDebugEnabled() || verbose) {
            // Using log.info(String, Object...) with {} placeholders doesn't work here, for some weird reason
            log.info(format(messageFormat, args));
        }
    }

    private boolean isInstantiableClass(final ClassFile classFile) {
        final String className = classFile.getName();
        if (classFile.isInterface()) {
            log.error(
                    "Found a type [{}] annotated as a component, but the type is not a concrete class."
                            + " NOT adding to index file!!",
                    className);
            return false;
        }
        if (classFile.isAbstract()) {
            log.error(
                    "Found a type [{}] annotated as a component, but the type is abstract."
                            + " NOT adding to index file!!",
                    className);
            return false;
        }
        // package-info files don't count either, but it's not an error to encounter one
        return !profileFinder.isPackageClass(classFile);
    }

    private boolean isInteresting(final String annotationType) {
        return super.acceptResult(annotationType) && springIndexWriter.isInteresting(annotationType);
    }

    /**
     * Visits the constructors of the class to see if they have any annotations of interest to the scanner.
     *
     * @param classFile the class we are inspecting
     * @param profiles the profiles that apply to the index
     */
    private void scanConstructors(final ClassFile classFile, final Set<String> profiles) {
        final String className = classFile.getName();
        final List<MethodInfo> methods = classFile.getMethods();
        methods.stream()
                .filter(MethodInfo::isConstructor)
                .forEach(constructor -> scanConstructor(profiles, className, constructor));
    }

    private void scanConstructor(final Set<String> profiles, final String className, final MethodInfo method) {
        // parameter 'names' in this case is actually parameter types
        final List<String> parameterTypes = getMetadataAdapter().getParameterNames(method);
        for (int i = 0; i < parameterTypes.size(); i++) {
            final String parameterType = parameterTypes.get(i);
            for (final Annotation parameterAnnotation : javassistHelper.getParameterAnnotations(method, i)) {
                scanConstructorParameterAnnotation(profiles, className, parameterType, parameterAnnotation);
            }
        }
    }

    private void scanConstructorParameterAnnotation(
            final Set<String> profiles,
            final String className,
            final String parameterType,
            final Annotation annotation) {
        final String annotationType = annotation.getTypeName();
        if (acceptResult(annotationType) && springIndexWriter.isParameterAnnotation(annotationType)) {
            final String nameFromAnnotation = javassistHelper.getAnnotationMember(annotation);
            log(
                    "\t(/) Found '%s' inside class '%s' method '<init>' parameter '%s'",
                    annotationType, className, parameterType);
            springIndexWriter.encounteredAnnotation(profiles, annotationType, nameFromAnnotation, parameterType);
            annotationValidator.encounteredAnnotation(annotationType, parameterType, className);
        }
    }

    private void scanFields(final ClassFile classFile, final Set<String> profiles) {
        final List<FieldInfo> fields = classFile.getFields();
        fields.forEach(field -> scanField(classFile, profiles, field));
    }

    private void scanField(final ClassFile classFile, final Set<String> profiles, final FieldInfo field) {
        final List<String> annotationTypes = new LinkedList<>();
        final String fieldName = field.getName();
        final AnnotationsAttribute annotations = (AnnotationsAttribute) field.getAttribute(visibleTag);
        if (annotations != null) {
            for (final Annotation annotation : annotations.getAnnotations()) {
                final String annotationType = annotation.getTypeName();
                annotationTypes.add(annotationType);
                if (acceptResult(annotationType) && springIndexWriter.isFieldAnnotation(annotationType)) {
                    final String fieldType = Descriptor.toClassName(field.getDescriptor());
                    final String nameFromAnnotation = javassistHelper.getAnnotationMember(annotation);
                    log(
                            "\t(/) Found '%s' inside class '%s' on field '%s' of type '%s'",
                            annotationType, classFile.getName(), fieldName, fieldType);
                    springIndexWriter.encounteredAnnotation(profiles, annotationType, nameFromAnnotation, fieldType);
                    annotationValidator.encounteredAnnotation(annotationType, fieldType, classFile.getName());
                }
            }
            if (annotationTypes.contains(ComponentImport.class.getCanonicalName())) {
                final List<String> productImportsPresentOnField = new ArrayList<>(KNOWN_PRODUCT_IMPORT_ANNOTATIONS);
                productImportsPresentOnField.retainAll(annotationTypes);
                if (!productImportsPresentOnField.isEmpty()) {
                    errors.add(String.format(
                            "ComponentImport annotation cannot be used with product specific"
                                    + " component imports: %s found on %s.%s",
                            Arrays.toString(productImportsPresentOnField.toArray()), classFile.getName(), fieldName));
                }
            }
        }
    }

    /**
     * Returns the number of classes actually scanned.
     *
     * @return see description
     */
    public int getClassesEncountered() {
        return classesEncountered;
    }

    /**
     * Returns the number of component classes encountered.
     *
     * @return see description
     */
    public int getComponentClassesEncountered() {
        return componentClassesEncountered;
    }

    /**
     * Returns any errors found while scanning.
     *
     * @return an empty list if scanning has not happened, or there were no errors
     */
    public List<String> getErrors() {
        return unmodifiableList(errors);
    }
}
