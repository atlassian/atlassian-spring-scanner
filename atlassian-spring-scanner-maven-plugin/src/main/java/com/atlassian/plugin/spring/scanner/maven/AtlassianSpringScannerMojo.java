package com.atlassian.plugin.spring.scanner.maven;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.dependency.graph.DependencyGraphBuilder;
import org.apache.maven.shared.dependency.graph.DependencyGraphBuilderException;
import org.apache.maven.shared.dependency.graph.DependencyNode;
import org.reflections.util.ClasspathHelper;
import org.slf4j.Logger;

import com.atlassian.plugin.spring.scanner.core.AtlassianSpringByteCodeScanner;
import com.atlassian.plugin.spring.scanner.core.AtlassianSpringByteCodeScanner.ScanResults;
import com.atlassian.plugin.spring.scanner.core.ByteCodeScannerConfiguration;

import static java.lang.Boolean.TRUE;
import static java.lang.String.format;
import static java.lang.String.join;
import static java.util.Collections.sort;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;
import static org.apache.maven.artifact.Artifact.SCOPE_TEST;
import static org.reflections.util.Utils.isEmpty;

/**
 * Maven plugin for atlassian-spring-scanning.
 * <p>
 * Use it by configuring the pom with:
 * <pre><code>
 * &lt;build&gt;
 *     &lt;plugins&gt;
 *         &lt;plugin&gt;
 *             &lt;groupId&gt;com.atlassian.plugins&lt;/groupId&gt;
 *             &lt;artifactId&gt;atlassian-spring-scanner-maven-plugin&lt;/artifactId&gt;
 *             &lt;version&gt;${project.version}#60;/version&gt;
 *             &lt;executions&gt;
 *                 &lt;execution&gt;
 *                     &lt;goals&gt;
 *                        &lt;goal&gt;atlassian-spring-scanner&lt;/goal&gt;
 *                     &lt;/goals&gt;
 *                     &lt;phase&gt;process-classes&lt;/phase&gt;
 *                  &lt;/execution&gt;
 *             &lt;/executions&gt;
 *             &lt;configuration&gt;
 *                 &lt;... optional configuration here&gt;
 *             &lt;/configuration&gt;
 *         &lt;/plugin&gt;
 *     &lt;/plugins&gt;
 * &lt;/build&gt;
 * </code></pre>
 */
@Mojo(
        name = "atlassian-spring-scanner",
        threadSafe = true,
        defaultPhase = LifecyclePhase.PREPARE_PACKAGE,
        requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class AtlassianSpringScannerMojo extends AbstractMojo {

    private static final String OUR_NAME = "Atlassian Spring Byte Code Scanner";
    private static final String DEFAULT_INCLUDE_EXCLUDE = "-java\\..*, -javax\\..*, -sun\\..*, -com\\.sun\\..*";

    @Parameter(defaultValue = "${project}", readonly = true)
    private MavenProject project;

    @Parameter(defaultValue = DEFAULT_INCLUDE_EXCLUDE)
    private String includeExclude;

    @Parameter(defaultValue = "false")
    private Boolean parallel;

    @Parameter(defaultValue = "false")
    private Boolean verbose;

    @Parameter(defaultValue = "false")
    private Boolean permitDuplicateImports;

    @Parameter()
    private List<Dependency> scannedDependencies = new ArrayList<>();

    @Component(hint = "default")
    private DependencyGraphBuilder dependencyGraphBuilder;

    public void execute() throws MojoExecutionException {
        getLog().info("Starting " + OUR_NAME + "...");
        getLog().info("");
        final long then = System.currentTimeMillis();

        final String outputDirectory = resolveOutputDirectory();
        if (!new File(outputDirectory).exists()) {
            getLog().warn(format("Skipping because %s was not found", outputDirectory));
            return;
        }

        warnInvalidScannedDependencies();

        final ByteCodeScannerConfiguration.Builder config = ByteCodeScannerConfiguration.builder()
                .setOutputDirectory(outputDirectory)
                .setClassPathUrls(parseUrls())
                .setIncludeExclude(includeExclude)
                .setLog(makeLogger())
                .setVerbose(verbose)
                .setPermitDuplicateImports(permitDuplicateImports);

        // go!
        final AtlassianSpringByteCodeScanner scanner = AtlassianSpringByteCodeScanner.getInstance(config.build());
        final ScanResults scanResults = scanner.scan();

        final long duration = System.currentTimeMillis() - then;
        getLog().info("");
        getLog().info(format("\tAnalysis ran in %d ms.", duration));
        getLog().info(format("\tEncountered %d total classes", scanResults.getClassesEncountered()));
        getLog().info(format("\tProcessed %d annotated classes", scanResults.getComponentClassesEncountered()));

        final List<String> errors = scanResults.getErrors();
        if (!errors.isEmpty()) {
            final String error = format(
                    "\t %d errors encountered during class analysis: %n\t %s", errors.size(), join("\n\t", errors));
            getLog().error(error);
            throw new IllegalStateException(error);
        }
    }

    private Logger makeLogger() {
        return new MavenLogAdapter(getLog());
    }

    private Collection<URL> parseUrls() throws MojoExecutionException {
        final Collection<URI> uris = new HashSet<>(); // collect a Set<URI> because Set<URL> is an anti-pattern
        final URI outputDirUrl = parseOutputDirUrl();
        uris.add(outputDirUrl);
        uris.addAll(getIncludedPackages());

        final Collection<Artifact> projectArtifacts = getProjectArtifacts();
        final Collection<Artifact> scannedArtifacts = resolveArtifacts(getScannedArtifacts(), projectArtifacts);
        final Iterable<Artifact> ignoredArtifacts = projectArtifacts.stream()
                .filter(artifact ->
                        scannedArtifacts.stream().noneMatch(scannedArtifact -> sameGAV(scannedArtifact, artifact)))
                .collect(toList());

        final Collection<URI> dependencyJars = getDependencyJars(scannedArtifacts);

        for (final Artifact artifact : ignoredArtifacts) {
            logVerbose(format(
                    "\t(X) Ignoring dependency for scanning %s:%s:%s",
                    artifact.getGroupId(), artifact.getArtifactId(), artifact.getScope()));
        }

        uris.addAll(dependencyJars);
        getLog().info("\t(/) The following directory will be scanned for annotations:");
        getLog().info(format("\t\t%s", outputDirUrl));
        if (!dependencyJars.isEmpty()) {
            getLog().info("");
            getLog().info("\t(/) The following dependencies will also be scanned for annotations: ");
            getLog().info("");
            for (final URI jar : dependencyJars) {
                getLog().info(format("\t\t%s", jar));
            }
        }

        return toUrls(uris);
    }

    private static Collection<URL> toUrls(final Collection<URI> uris) throws MojoExecutionException {
        final Collection<URL> urls = new ArrayList<>();
        for (final URI uri : uris) {
            try {
                urls.add(uri.toURL());
            } catch (MalformedURLException e) {
                throw new MojoExecutionException("Invalid URI " + uri, e);
            }
        }
        return urls;
    }

    private Collection<URI> getIncludedPackages() throws MojoExecutionException {
        final Collection<URI> uris = new ArrayList<>();
        if (!isEmpty(includeExclude)) {
            for (final String string : includeExclude.split(",")) {
                final String trimmed = string.trim();
                final char prefix = trimmed.charAt(0);
                if (prefix == '+') {
                    final String pattern = trimmed.substring(1);
                    logVerbose(format("\tAdding include / exclude %s", prefix));
                    for (final URL url : ClasspathHelper.forPackage(pattern)) {
                        uris.add(toUri(url));
                    }
                }
            }
        }
        return uris;
    }

    private static URI toUri(final URL url) throws MojoExecutionException {
        try {
            return url.toURI();
        } catch (final URISyntaxException e) {
            throw new MojoExecutionException("Invalid URL " + url, e);
        }
    }

    private Collection<URI> getDependencyJars(final Collection<Artifact> scannedArtifacts) {
        scannedArtifacts.forEach(artifact -> logVerbose(format(
                "\t(/) Including dependency for scanning %s:%s:%s",
                artifact.getGroupId(), artifact.getArtifactId(), artifact.getScope())));
        return scannedArtifacts.stream()
                .map(Artifact::getFile)
                .map(File::toURI)
                .collect(toCollection(LinkedHashSet::new));
    }

    private void logVerbose(final String message) {
        if (TRUE.equals(verbose)) {
            getLog().info(message);
        }
    }

    private static boolean isSensibleScope(final Artifact artifact) {
        return !SCOPE_TEST.equals(artifact.getScope());
    }

    private URI parseOutputDirUrl() {
        return new File(resolveOutputDirectory() + '/').toURI();
    }

    private String resolveOutputDirectory() {
        return getProject().getBuild().getOutputDirectory();
    }

    private MavenProject getProject() {
        return project;
    }

    private static boolean sameGAV(final Artifact artifact1, final Artifact artifact2) {
        return artifact1.getGroupId().equals(artifact2.getGroupId())
                && artifact1.getArtifactId().equals(artifact2.getArtifactId())
                && artifact1.getVersion().equals(artifact2.getVersion());
    }

    /**
     * The artifacts in the project list are resolved, the scanned ones are not but rather are logical, hence we
     * have to make them real.
     *
     * @param scannedArtifacts the GA artifacts we want scanned
     * @param projectArtifacts the resolved list including the above
     * @return the resolved versions of the scanned artifacts
     */
    private static List<Artifact> resolveArtifacts(
            final Collection<Artifact> scannedArtifacts, final Collection<Artifact> projectArtifacts) {
        return scannedArtifacts.stream()
                .map(artifact -> projectArtifacts.stream()
                        .filter(projectArtifact -> sameGAV(projectArtifact, artifact))
                        .findFirst()
                        .orElseThrow(() -> new RuntimeException("Unable to find " + artifact)))
                .collect(toList());
    }

    private DependencyNode getDependencyGraph(final MavenProject project) {
        try {
            return dependencyGraphBuilder.buildDependencyGraph(project, AtlassianSpringScannerMojo::isSensibleScope);
        } catch (DependencyGraphBuilderException e) {
            throw new IllegalStateException(e);
        }
    }

    private List<Artifact> getProjectArtifacts() {
        final List<Artifact> artifacts = new ArrayList<>(project.getArtifacts());
        sort(artifacts);
        return artifacts;
    }

    private List<Artifact> getScannedArtifacts() {
        final DependencyNode dependencyGraph = getDependencyGraph(project);
        return ScannedDependencyArtifactBuilder.buildScannedArtifacts(dependencyGraph, scannedDependencies);
    }

    private void warnInvalidScannedDependencies() {
        scannedDependencies.stream()
                .map(Dependency::getArtifactId)
                .filter(AtlassianSpringScannerMojo::isPartialWildcard)
                .forEach(artifactId -> getLog().warn(format(
                        "Invalid artifact ID %s in scannedDependencies. Partial wildcards are not supported.",
                        artifactId)));
    }

    private static boolean isPartialWildcard(final String artifactId) {
        return artifactId.contains("*") && !"*".equals(artifactId);
    }
}
