package com.atlassian.plugin.spring.scanner.maven;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.model.Dependency;
import org.apache.maven.model.Exclusion;
import org.apache.maven.shared.dependency.graph.DependencyNode;
import org.apache.maven.shared.dependency.graph.filter.DependencyNodeFilter;
import org.apache.maven.shared.dependency.graph.traversal.CollectingDependencyNodeVisitor;
import org.apache.maven.shared.dependency.graph.traversal.DependencyNodeVisitor;
import org.apache.maven.shared.dependency.graph.traversal.FilteringDependencyNodeVisitor;

import static java.util.stream.Collectors.toList;

/**
 * Finds all transitive dependencies of the built artifact and cross-references them to the named
 * scanned deps and includes any transitive deps of those named scanned dependencies by default, UNLESS
 * there are 'exclusions' in the configuration.
 */
final class ScannedDependencyArtifactBuilder {

    static List<Artifact> buildScannedArtifacts(
            final DependencyNode projectDependencyGraph, final List<Dependency> configuredScannedDependencies) {
        final List<Artifact> artifacts = new ArrayList<>();

        projectDependencyGraph.accept(new DependencyNodeVisitor() {
            @Override
            public boolean visit(final DependencyNode node) {
                return true;
            }

            @Override
            public boolean endVisit(final DependencyNode node) {
                final Artifact artifact = node.getArtifact();
                artifactInConfiguredList(artifact, configuredScannedDependencies)
                        .ifPresent(dependency -> artifacts.addAll(findIncludedTransitiveDeps(node, dependency)));
                return true;
            }
        });
        return artifacts;
    }

    private static Optional<Dependency> artifactInConfiguredList(
            final Artifact artifact, final List<Dependency> configuredScannedDependencies) {
        return configuredScannedDependencies.stream()
                .filter(dependency -> matchesIncludingWildcards(artifact, dependency))
                .findFirst();
    }

    private static boolean matchesIncludingWildcards(final Artifact artifact, final Dependency dependency) {
        if ("*".equals(dependency.getArtifactId())) {
            return dependency.getGroupId().equals(artifact.getGroupId());
        } else {
            return dependency.getGroupId().equals(artifact.getGroupId())
                    && dependency.getArtifactId().equals(artifact.getArtifactId());
        }
    }

    /**
     * This method descends the specified node and adds all transitive dependencies to the list of artifacts UNLESS the
     * configuration says that there are exclusions
     *
     * @param dependencyNode the graph node
     * @param dependency     the configuration dependency from maven config
     *
     * @return a list of included transitive deps
     */
    private static List<Artifact> findIncludedTransitiveDeps(
            final DependencyNode dependencyNode, final Dependency dependency) {
        // we want all the transitive deps to be scanned as long as they are not explicitly excluded
        final DependencyNodeFilter includedDeps = node -> !isExcluded(node.getArtifact(), dependency.getExclusions());
        final CollectingDependencyNodeVisitor collector = new CollectingDependencyNodeVisitor();
        final FilteringDependencyNodeVisitor filterer = new FilteringDependencyNodeVisitor(collector, includedDeps);
        dependencyNode.accept(filterer);
        return collector.getNodes().stream().map(DependencyNode::getArtifact).collect(toList());
    }

    private static boolean isExcluded(final Artifact artifact, final Collection<Exclusion> exclusions) {
        return exclusions.stream().anyMatch(exclusion -> isSameGroupAndArtifactId(exclusion, artifact));
    }

    private static boolean isSameGroupAndArtifactId(final Exclusion exclusion, final Artifact artifact) {
        return exclusion.getGroupId().equals(artifact.getGroupId())
                && exclusion.getArtifactId().equals(artifact.getArtifactId());
    }

    private ScannedDependencyArtifactBuilder() {}
}
