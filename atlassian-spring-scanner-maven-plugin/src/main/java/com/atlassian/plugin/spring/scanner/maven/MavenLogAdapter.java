package com.atlassian.plugin.spring.scanner.maven;

import java.text.MessageFormat;

import org.apache.maven.plugin.logging.Log;
import org.slf4j.Logger;
import org.slf4j.Marker;

/**
 * A wrapper to send sl4j to the maven mavenLog.  Markers are not supported
 * and trace == debug
 */
class MavenLogAdapter implements Logger {
    private Log mavenLog;

    MavenLogAdapter(Log mavenLog) {
        this.mavenLog = mavenLog;
    }

    private String msgFmt(String format, Object... args) {
        return MessageFormat.format(format, args);
    }

    public String getName() {
        return mavenLog.getClass().getName();
    }

    public boolean isDebugEnabled() {
        return mavenLog.isDebugEnabled();
    }

    public void debug(String format, Object arg1, Object arg2) {
        if (mavenLog.isDebugEnabled()) {
            mavenLog.debug(msgFmt(format, arg1, arg2));
        }
    }

    public void debug(String format, Object arg) {
        if (mavenLog.isDebugEnabled()) {
            mavenLog.debug(msgFmt(format, arg));
        }
    }

    public void debug(String format, Object[] argArray) {
        if (mavenLog.isDebugEnabled()) {
            mavenLog.debug(msgFmt(format, argArray));
        }
    }

    public void debug(String msg, Throwable t) {
        if (mavenLog.isDebugEnabled()) {
            mavenLog.debug(msg, t);
        }
    }

    public void debug(String msg) {
        if (mavenLog.isDebugEnabled()) {
            mavenLog.debug(msg);
        }
    }

    public boolean isErrorEnabled() {
        return mavenLog.isErrorEnabled();
    }

    public void error(String format, Object arg1, Object arg2) {
        if (mavenLog.isErrorEnabled()) {
            mavenLog.error(msgFmt(format, arg1, arg2));
        }
    }

    public void error(String format, Object arg) {
        if (mavenLog.isErrorEnabled()) {
            mavenLog.error(msgFmt(format, arg));
        }
    }

    public void error(String format, Object[] argArray) {
        if (mavenLog.isErrorEnabled()) {
            mavenLog.error(msgFmt(format, argArray));
        }
    }

    public void error(String msg, Throwable t) {
        if (mavenLog.isErrorEnabled()) {
            mavenLog.error(msg, t);
        }
    }

    public void error(String msg) {
        if (mavenLog.isErrorEnabled()) {
            mavenLog.error(msg);
        }
    }

    public boolean isInfoEnabled() {
        return mavenLog.isInfoEnabled();
    }

    public void info(String format, Object arg1, Object arg2) {
        if (mavenLog.isInfoEnabled()) {
            mavenLog.info(msgFmt(format, arg1, arg2));
        }
    }

    public void info(String format, Object arg) {
        if (mavenLog.isInfoEnabled()) {
            mavenLog.info(msgFmt(format, arg));
        }
    }

    public void info(String format, Object[] argArray) {
        if (mavenLog.isInfoEnabled()) {
            mavenLog.info(msgFmt(format, argArray));
        }
    }

    public void info(String msg, Throwable t) {
        if (mavenLog.isInfoEnabled()) {
            mavenLog.info(msg, t);
        }
    }

    public void info(String msg) {
        if (mavenLog.isInfoEnabled()) {
            mavenLog.info(msg);
        }
    }

    public boolean isTraceEnabled() {
        return mavenLog.isDebugEnabled();
    }

    public void trace(String format, Object arg1, Object arg2) {
        debug(format, arg1, arg2);
    }

    public void trace(String format, Object arg) {
        debug(format, arg);
    }

    public void trace(String format, Object[] argArray) {
        debug(format, argArray);
    }

    public void trace(String msg, Throwable t) {
        debug(msg, t);
    }

    public void trace(String msg) {
        debug(msg);
    }

    public boolean isWarnEnabled() {
        return mavenLog.isWarnEnabled();
    }

    public void warn(String format, Object arg1, Object arg2) {
        if (mavenLog.isWarnEnabled()) {
            mavenLog.warn(msgFmt(format, arg1, arg2));
        }
    }

    public void warn(String format, Object arg) {
        if (mavenLog.isWarnEnabled()) {
            mavenLog.warn(msgFmt(format, arg));
        }
    }

    public void warn(String format, Object[] argArray) {
        if (mavenLog.isWarnEnabled()) {
            mavenLog.warn(msgFmt(format, argArray));
        }
    }

    public void warn(String msg, Throwable t) {
        if (mavenLog.isWarnEnabled()) {
            mavenLog.warn(msg, t);
        }
    }

    public void warn(String msg) {
        if (mavenLog.isWarnEnabled()) {
            mavenLog.warn(msg);
        }
    }

    public void debug(Marker marker, String format, Object arg1, Object arg2) {
        debug(format, arg1, arg2);
    }

    public boolean isTraceEnabled(Marker marker) {
        return isDebugEnabled();
    }

    public void trace(Marker marker, String msg) {
        debug(msg);
    }

    public void trace(Marker marker, String format, Object arg) {
        debug(format, arg);
    }

    public void trace(Marker marker, String format, Object arg1, Object arg2) {
        debug(format, arg1, arg2);
    }

    public void trace(Marker marker, String format, Object... argArray) {
        debug(marker, format, argArray);
    }

    public void trace(Marker marker, String msg, Throwable t) {
        debug(msg, t);
    }

    public boolean isDebugEnabled(Marker marker) {
        return isDebugEnabled();
    }

    public void debug(Marker marker, String msg) {
        debug(msg);
    }

    public void debug(Marker marker, String format, Object arg) {
        debug(format, arg);
    }

    public void debug(Marker marker, String format, Object... arguments) {
        debug(format, arguments);
    }

    public void debug(Marker marker, String msg, Throwable t) {
        debug(msg, t);
    }

    public boolean isInfoEnabled(Marker marker) {
        return isInfoEnabled();
    }

    public void info(Marker marker, String msg) {
        info(msg);
    }

    public void info(Marker marker, String format, Object arg) {
        info(format, arg);
    }

    public void info(Marker marker, String format, Object arg1, Object arg2) {
        info(format, arg1, arg2);
    }

    public void info(Marker marker, String format, Object... arguments) {
        info(format, arguments);
    }

    public void info(Marker marker, String msg, Throwable t) {
        info(msg, t);
    }

    public boolean isWarnEnabled(Marker marker) {
        return isWarnEnabled();
    }

    public void warn(Marker marker, String msg) {
        warn(msg);
    }

    public void warn(Marker marker, String format, Object arg) {
        warn(format, arg);
    }

    public void warn(Marker marker, String format, Object arg1, Object arg2) {
        warn(format, arg1, arg2);
    }

    public void warn(Marker marker, String format, Object... arguments) {
        warn(format, arguments);
    }

    public void warn(Marker marker, String msg, Throwable t) {
        warn(msg, t);
    }

    public boolean isErrorEnabled(Marker marker) {
        return isErrorEnabled();
    }

    public void error(Marker marker, String msg) {
        error(msg);
    }

    public void error(Marker marker, String format, Object arg) {
        error(format, arg);
    }

    public void error(Marker marker, String format, Object arg1, Object arg2) {
        error(format, arg1, arg2);
    }

    public void error(Marker marker, String format, Object... arguments) {
        error(format, arguments);
    }

    public void error(Marker marker, String msg, Throwable t) {
        error(msg, t);
    }
}
