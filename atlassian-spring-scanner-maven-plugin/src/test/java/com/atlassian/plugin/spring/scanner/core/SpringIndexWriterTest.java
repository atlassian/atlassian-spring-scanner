package com.atlassian.plugin.spring.scanner.core;

import java.io.File;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Comparator.comparing;
import static org.codehaus.plexus.util.FileUtils.fileRead;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import static com.atlassian.plugin.spring.scanner.util.CommonConstants.INDEX_FILES_DIR;

public class SpringIndexWriterTest {

    private SpringIndexWriter indexWriter;

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Before
    public void setUp() {
        indexWriter = new SpringIndexWriter(temporaryFolder.getRoot().getAbsolutePath());
    }

    private void assertInteresting(final Class<? extends Annotation> annotationClass, final boolean expectedResult) {
        assertThat(indexWriter.isInteresting(annotationClass.getName()), is(expectedResult));
    }

    @Test
    public void isInteresting_whenPassedNull_shouldReturnFalse() {
        assertThat(indexWriter.isInteresting(null), is(false));
    }

    @Test
    public void isInteresting_whenPassedUninterestingType_shouldReturnFalse() {
        assertInteresting(Nonnull.class, false);
    }

    @Test
    public void isInteresting_whenPassedInterestingType_shouldReturnTrue() {
        assertInteresting(ComponentImport.class, true);
    }

    private void assertParameterOrFieldAnnotation(
            final Class<? extends Annotation> annotationClass, final boolean expectedResult) {
        assertThat(indexWriter.isParameterAnnotation(annotationClass.getName()), is(expectedResult));
        assertThat(indexWriter.isFieldAnnotation(annotationClass.getName()), is(expectedResult));
    }

    @Test
    public void isParameterAnnotation_whenPassedNull_shouldReturnFalse() {
        assertThat(indexWriter.isParameterAnnotation(null), is(false));
    }

    @Test
    public void isFieldAnnotation_whenPassedNull_shouldReturnFalse() {
        assertThat(indexWriter.isFieldAnnotation(null), is(false));
    }

    @Test
    public void isParameterOrFieldAnnotation_whenPassedUninterestingAnnotation_shouldReturnFalse() {
        assertParameterOrFieldAnnotation(Nonnull.class, false);
    }

    @Test
    public void isParameterOrFieldAnnotation_whenPassedNonMatchingAnnotation_shouldReturnFalse() {
        assertParameterOrFieldAnnotation(ModuleType.class, false);
    }

    @Test
    public void isParameterOrFieldAnnotation_whenPassedMatchingAnnotation_shouldReturnTrue() {
        assertParameterOrFieldAnnotation(ComponentImport.class, true);
    }

    private List<File> getIndexFiles() {
        final File indexFilesDirectory = new File(temporaryFolder.getRoot(), INDEX_FILES_DIR);
        final File[] fileArray = indexFilesDirectory.listFiles();
        if (fileArray == null) {
            return emptyList();
        }
        final List<File> fileList = asList(fileArray);
        fileList.sort(comparing(File::getName));
        return fileList;
    }

    @Test
    public void writeIndexes_shouldWriteNoFiles_whenNoAnnotationsWereRecorded() {
        // Act
        indexWriter.writeIndexes();

        // Assert
        assertThat(getIndexFiles(), is(emptyList()));
    }

    private static void assertFile(final File actualFile, final String expectedName, final String expectedContents)
            throws Exception {
        assertThat(actualFile.getName(), is(expectedName));
        assertThat(fileRead(actualFile, UTF_8.name()), is(expectedContents));
    }

    @Test
    public void writeIndexes_shouldWriteTwoFiles_whenTwoAnnotationsWereRecorded() throws Exception {
        // Arrange
        final Set<String> targetProfiles = emptySet();
        final String annotation1 = ComponentImport.class.getName();
        final String annotation2 = ExportAsService.class.getName();
        final String componentClass = "com.example.Thing";
        indexWriter.encounteredAnnotation(targetProfiles, annotation1, "theName", componentClass);
        indexWriter.encounteredAnnotation(targetProfiles, annotation1, null, componentClass);
        indexWriter.encounteredAnnotation(targetProfiles, annotation2, null, componentClass);

        // Act
        indexWriter.writeIndexes();

        // Assert
        final List<File> indexFiles = getIndexFiles();
        assertThat(indexFiles, hasSize(2));
        assertFile(indexFiles.get(0), "exports", "com.example.Thing\n");
        assertFile(indexFiles.get(1), "imports", "com.example.Thing\ncom.example.Thing#theName\n");
    }
}
