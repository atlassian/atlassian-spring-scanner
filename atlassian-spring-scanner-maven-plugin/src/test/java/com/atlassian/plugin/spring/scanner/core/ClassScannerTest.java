package com.atlassian.plugin.spring.scanner.core;

import java.util.Set;
import javassist.bytecode.ClassFile;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.annotation.Annotation;

import org.springframework.stereotype.Component;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.reflections.Configuration;
import org.reflections.Store;
import org.reflections.adapters.MetadataAdapter;
import org.slf4j.Logger;

import com.atlassian.plugin.spring.scanner.annotation.export.ModuleType;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import static com.atlassian.plugin.spring.scanner.util.CommonConstants.HOST_CONTAINER_CLASS;

public class ClassScannerTest {

    private static final boolean VERBOSE = true;

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private AnnotationValidator annotationValidator;

    @Mock
    private Configuration configuration;

    @Mock
    private JavassistHelper javassistHelper;

    @Mock
    private Logger log;

    @Mock
    private MetadataAdapter<Object, Object, Object> metadataAdapter;

    @Mock
    private ProfileFinder profileFinder;

    @Mock
    private SpringIndexWriter springIndexWriter;

    @Mock
    private Store store;

    private ClassScanner classScanner;

    @Before
    public void setUp() {
        classScanner =
                new ClassScanner(annotationValidator, javassistHelper, log, profileFinder, springIndexWriter, VERBOSE);
        classScanner.setConfiguration(configuration);
        when(configuration.getMetadataAdapter()).thenReturn(metadataAdapter);
    }

    @Test
    public void scan_whenClassHasNoAnnotationsConstructorsOrFields_shouldReturnEmptyResults() {
        // Arrange
        final ClassFile classFile = newClassFile(false, "emptyClass");
        when(metadataAdapter.getClassAnnotationNames(any())).thenReturn(emptyList());

        // Act
        classScanner.scan(classFile, store);

        // Assert
        assertThat(classScanner.getClassesEncountered(), is(1));
        assertThat(classScanner.getComponentClassesEncountered(), is(0));
        assertThat(classScanner.getErrors(), is(emptyList()));
        verifyNoMoreInteractions(store);
    }

    @Test
    public void scan_whenClassHasNoInterestingAnnotations_shouldReturnEmptyResults() {
        // Arrange
        final ClassFile classFile = newClassFile(false, "theClass");
        final String annotation = "uninteresting";
        when(metadataAdapter.getClassAnnotationNames(any())).thenReturn(singletonList(annotation));
        when(springIndexWriter.isInteresting(annotation)).thenReturn(false);

        // Act
        classScanner.scan(classFile, store);

        // Assert
        assertThat(classScanner.getClassesEncountered(), is(1));
        assertThat(classScanner.getComponentClassesEncountered(), is(0));
        assertThat(classScanner.getErrors(), is(emptyList()));
        verifyNoMoreInteractions(store);
    }

    @Test
    public void scan_whenClassIsAnInterface_shouldReturnEmptyResultsAndLogAnInfoMessage() {
        // Arrange
        final String className = "anInterface";
        final ClassFile classFile = newClassFile(true, className);
        final String annotation = "soInteresting";
        when(metadataAdapter.getClassAnnotationNames(any())).thenReturn(singletonList(annotation));
        when(springIndexWriter.isInteresting(annotation)).thenReturn(true);
        when(metadataAdapter.getClassName(classFile)).thenReturn(className);

        // Act
        classScanner.scan(classFile, store);

        // Assert
        assertThat(classScanner.getClassesEncountered(), is(1));
        assertThat(classScanner.getComponentClassesEncountered(), is(0));
        assertThat(classScanner.getErrors(), is(emptyList()));
        verifyNoMoreInteractions(store);
        verify(log).info("\t\t(X) Class not suitable 'anInterface' for annotation 'soInteresting'");
    }

    @Test
    public void scan_whenClassIsInstantiable_shouldRegisterItWithSpringIndexWriterAndAnnotationValidator() {
        // Arrange
        final String className = "aConcreteClass";
        final ClassFile classFile = newClassFile(false, className);
        final String annotation = "soInteresting";
        final String annotatedName = "theAnnotatedName";
        final Set<String> profiles = singleton("theProfile");
        when(metadataAdapter.getClassAnnotationNames(any())).thenReturn(singletonList(annotation));
        when(springIndexWriter.isInteresting(annotation)).thenReturn(true);
        when(metadataAdapter.getClassName(classFile)).thenReturn(className);
        when(javassistHelper.getAnnotationMember(classFile, annotation)).thenReturn(annotatedName);
        when(profileFinder.getProfiles(classFile)).thenReturn(profiles);

        // Act
        classScanner.scan(classFile, store);

        // Assert
        assertThat(classScanner.getClassesEncountered(), is(1));
        assertThat(classScanner.getComponentClassesEncountered(), is(1));
        assertThat(classScanner.getErrors(), is(emptyList()));
        verifyNoMoreInteractions(store);
        verify(log)
                .info(
                        "\t(/) Found annotation 'soInteresting' inside class 'aConcreteClass' with name 'theAnnotatedName'");
        verify(springIndexWriter).encounteredAnnotation(profiles, annotation, annotatedName, className);
        verify(annotationValidator).encounteredAnnotation(annotation, className, className);
    }

    @Test
    public void scan_whenClassIsModuleType_shouldAlsoRegisterHostContainerComponent() {
        // Arrange
        final String className = "aModuleType";
        final ClassFile classFile = newClassFile(false, className);
        final String annotation = ModuleType.class.getCanonicalName();
        final Set<String> profiles = singleton("theProfile");
        when(metadataAdapter.getClassAnnotationNames(any())).thenReturn(singletonList(annotation));
        when(springIndexWriter.isInteresting(annotation)).thenReturn(true);
        when(metadataAdapter.getClassName(classFile)).thenReturn(className);
        when(javassistHelper.getAnnotationMember(classFile, annotation)).thenReturn(null);
        when(profileFinder.getProfiles(classFile)).thenReturn(profiles);

        // Act
        classScanner.scan(classFile, store);

        // Assert
        verify(springIndexWriter).encounteredAnnotation(profiles, annotation, null, className);
        verify(springIndexWriter)
                .encounteredAnnotation(profiles, Component.class.getCanonicalName(), "", HOST_CONTAINER_CLASS);
    }

    @Test
    public void scan_whenClassHasOneNoArgConstructor_shouldIgnoreIt() {
        // Arrange
        final String className = "classWithConstructor";
        final ClassFile classFile = newClassFile(false, className);
        final Set<String> profiles = singleton("theProfile");
        final MethodInfo method = mock(MethodInfo.class);
        classFile.addMethod2(method);
        when(method.isConstructor()).thenReturn(true);
        when(metadataAdapter.getParameterNames(method)).thenReturn(emptyList());
        when(profileFinder.getProfiles(classFile)).thenReturn(profiles);

        // Act
        classScanner.scan(classFile, store);

        // Assert
        assertThat(classScanner.getClassesEncountered(), is(1));
        assertThat(classScanner.getComponentClassesEncountered(), is(0));
        assertThat(classScanner.getErrors(), is(emptyList()));
        verifyNoMoreInteractions(annotationValidator, springIndexWriter, store);
    }

    @Test
    public void scan_whenConstructorHasNoAnnotatedParameters_shouldIgnoreIt() {
        // Arrange
        final String className = "classWithConstructor";
        final ClassFile classFile = newClassFile(false, className);
        final Set<String> profiles = singleton("theProfile");
        final MethodInfo method = mock(MethodInfo.class);
        classFile.addMethod2(method);
        when(method.isConstructor()).thenReturn(true);
        when(metadataAdapter.getParameterNames(method)).thenReturn(asList("type1", "type2"));
        when(profileFinder.getProfiles(classFile)).thenReturn(profiles);
        when(javassistHelper.getParameterAnnotations(same(method), anyInt())).thenReturn(emptyList());

        // Act
        classScanner.scan(classFile, store);

        // Assert
        assertThat(classScanner.getClassesEncountered(), is(1));
        assertThat(classScanner.getComponentClassesEncountered(), is(0));
        assertThat(classScanner.getErrors(), is(emptyList()));
        verifyNoMoreInteractions(annotationValidator, springIndexWriter, store);
    }

    @Test
    public void scan_whenConstructorHasComponentImportParameter_shouldRegisterIt() {
        // Arrange
        final String className = "classWithConstructor";
        final ClassFile classFile = newClassFile(false, className);
        final Set<String> profiles = singleton("theProfile");
        final MethodInfo method = mock(MethodInfo.class);
        final Annotation parameterAnnotation = mock(Annotation.class);
        final String annotationType = ComponentImport.class.getCanonicalName();
        final String beanName = "theImportedBean";
        when(springIndexWriter.isParameterAnnotation(annotationType)).thenReturn(true);
        when(parameterAnnotation.getTypeName()).thenReturn(annotationType);
        classFile.addMethod2(method);
        when(method.isConstructor()).thenReturn(true);
        final String parameterType = "theParameterType";
        when(metadataAdapter.getParameterNames(method)).thenReturn(singletonList(parameterType));
        when(profileFinder.getProfiles(classFile)).thenReturn(profiles);
        when(javassistHelper.getParameterAnnotations(method, 0)).thenReturn(singletonList(parameterAnnotation));
        when(javassistHelper.getAnnotationMember(parameterAnnotation)).thenReturn(beanName);

        // Act
        classScanner.scan(classFile, store);

        // Assert
        assertThat(classScanner.getClassesEncountered(), is(1));
        assertThat(classScanner.getComponentClassesEncountered(), is(0));
        assertThat(classScanner.getErrors(), is(emptyList()));
        verify(springIndexWriter).encounteredAnnotation(profiles, annotationType, beanName, parameterType);
        verify(annotationValidator).encounteredAnnotation(annotationType, parameterType, className);
        verify(log)
                .info("\t(/) Found 'com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport' inside"
                        + " class 'classWithConstructor' method '<init>' parameter 'theParameterType'");
        verifyNoMoreInteractions(store);
    }

    private static ClassFile newClassFile(final boolean isInterface, final String classname) {
        // final => can't be mocked
        return new ClassFile(isInterface, classname, null);
    }
}
