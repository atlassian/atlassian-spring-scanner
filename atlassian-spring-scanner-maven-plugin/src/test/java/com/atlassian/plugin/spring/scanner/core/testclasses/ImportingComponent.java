package com.atlassian.plugin.spring.scanner.core.testclasses;

import org.springframework.stereotype.Component;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

@Component
public class ImportingComponent {
    @ComponentImport
    private DeclaredComponent declaringComponent;
}
