package com.atlassian.plugin.spring.scanner.core;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AtlassianSpringByteCodeScannerTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private AnnotationValidator annotationValidator;

    @Mock
    private ByteCodeScannerConfiguration configuration;

    @Mock
    private ClassScanner classScanner;

    @Mock
    private SpringIndexWriter springIndexWriter;

    @InjectMocks
    private AtlassianSpringByteCodeScanner scanner;

    @Test
    public void scan_whenValidationEnabled_shouldIncludeValidationErrors() {
        // Arrange
        setUpScanningErrors();
        setUpValidationErrors();
        when(configuration.shouldValidate()).thenReturn(true);

        // Act
        final AtlassianSpringByteCodeScanner.ScanResults results = scanner.scan();

        // Assert
        assertThat(results.getErrors(), contains("error1", "error2", "error3", "error4"));
        verify(springIndexWriter).writeIndexes();
    }

    @Test
    public void scan_whenValidationDisabled_shouldNotIncludeValidationErrors() {
        // Arrange
        setUpScanningErrors();
        setUpValidationErrors();
        when(configuration.shouldValidate()).thenReturn(false);

        // Act
        final AtlassianSpringByteCodeScanner.ScanResults results = scanner.scan();

        // Assert
        assertThat(results.getErrors(), contains("error1", "error2"));
        verify(springIndexWriter).writeIndexes();
    }

    @Test
    public void scan_shouldReportScannedClassCounts() {
        // Arrange
        final int classesEncountered = 42;
        final int componentClassesEncountered = classesEncountered - 1; // to be different
        when(classScanner.getClassesEncountered()).thenReturn(classesEncountered);
        when(classScanner.getComponentClassesEncountered()).thenReturn(componentClassesEncountered);

        // Act
        final AtlassianSpringByteCodeScanner.ScanResults results = scanner.scan();

        // Assert
        assertThat(results.getClassesEncountered(), is(classesEncountered));
        assertThat(results.getComponentClassesEncountered(), is(componentClassesEncountered));
    }

    private void setUpScanningErrors() {
        when(classScanner.getErrors()).thenReturn(asList("error1", "error2"));
    }

    private void setUpValidationErrors() {
        when(annotationValidator.validate()).thenReturn(asList("error3", "error4"));
    }
}
