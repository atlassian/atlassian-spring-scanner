package com.atlassian.plugin.spring.scanner.core;

import java.net.URL;

import org.junit.Test;

import com.atlassian.plugin.spring.scanner.core.AtlassianSpringByteCodeScanner.ScanResults;
import com.atlassian.plugin.spring.scanner.core.testclasses.DeclaredComponent;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

/**
 * JUnit test using real collaborating objects.
 */
public class AtlassianSpringByteCodeScannerIntegrationTest {

    // Creates a scanner config to scan the testclasses package.
    private static ByteCodeScannerConfiguration createScannerConfiguration(final boolean permitDuplicateImports) {
        final URL testclassesPackage = DeclaredComponent.class.getResource("");
        return ByteCodeScannerConfiguration.builder()
                .setVerbose(false)
                .setPermitDuplicateImports(permitDuplicateImports)
                .setClassPathUrls(singletonList(testclassesPackage))
                .build();
    }

    private static void assertScanResults(final boolean permitDuplicateImports, final int expectedErrors) {
        final ByteCodeScannerConfiguration config = createScannerConfiguration(permitDuplicateImports);
        final AtlassianSpringByteCodeScanner scanner = AtlassianSpringByteCodeScanner.getInstance(config);
        final ScanResults results = scanner.scan();
        assertThat(results.getClassesEncountered(), is(2));
        assertThat(results.getComponentClassesEncountered(), is(2));
        assertThat(results.getErrors(), hasSize(expectedErrors));
    }

    @Test
    public void scan_whenDuplicateImportsNotPermitted_shouldReportAnError() {
        assertScanResults(false, 1);
    }

    @Test
    public void scan_whenDuplicateImportsArePermitted_shouldReportNoErrors() {
        assertScanResults(true, 0);
    }
}
