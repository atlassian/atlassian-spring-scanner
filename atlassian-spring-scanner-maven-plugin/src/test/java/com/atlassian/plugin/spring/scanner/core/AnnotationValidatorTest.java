package com.atlassian.plugin.spring.scanner.core;

import java.util.List;
import javax.annotation.Nonnull;

import org.springframework.stereotype.Component;
import org.junit.Before;
import org.junit.Test;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import static java.util.Collections.emptyList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AnnotationValidatorTest {

    private AnnotationValidator annotationValidator;

    @Before
    public void setUp() {
        this.annotationValidator = new AnnotationValidator();
    }

    @Test
    public void encounteredAnnotation_whenAnnotationTypeIsIrrelevant_shouldNotReportAnError() {
        // Act
        annotationValidator.encounteredAnnotation(Nonnull.class.getName(), "theComponent", "theLocation");

        // Assert
        assertThat(annotationValidator.validate(), is(emptyList()));
    }

    @Test
    public void validate_whenComponentBothCreatedAndImported_shouldReportError() {
        // Arrange
        final String component = "theComponent";
        annotationValidator.encounteredAnnotation(Component.class.getName(), component, "location1");
        annotationValidator.encounteredAnnotation(ComponentImport.class.getName(), component, "location2");

        // Act
        final List<String> errors = annotationValidator.validate();

        // Assert
        assertThat(
                errors,
                contains("Cannot import a component within a plugin that declares the same component."
                        + " Component 'theComponent' was declared in [location1] but imported in [location2]"));
    }
}
