package com.atlassian.plugin.spring.scanner.core;

import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;
import javax.annotation.Nullable;

import org.springframework.stereotype.Component;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.atlassian.plugin.spring.scanner.core.SpringIndexWriter.MeaningfulAnnotation;
import com.atlassian.plugin.spring.scanner.core.SpringIndexWriter.RecordedAnnotationsHolder;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toSet;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class RecordedAnnotationsHolderTest {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void getRecordedAnnotations_whenNothingRecorded_shouldReturnAnEmptyMap() {
        // Arrange
        final RecordedAnnotationsHolder annotationsHolder = new RecordedAnnotationsHolder();

        // Act
        final Map<?, ?> recordedAnnotations = annotationsHolder.getRecordedAnnotations();

        // Assert
        assertThat(recordedAnnotations, is(emptyMap()));
    }

    @Test
    public void getRecordedAnnotations_whenOneInterestingAnnotationRecordedWithName_shouldReturnMapWithOneEntry() {
        // Arrange
        final RecordedAnnotationsHolder annotationsHolder = new RecordedAnnotationsHolder();
        annotationsHolder.recordAnnotation(Component.class.getName(), "myName", "com.example.Foo");

        // Act
        final Map<MeaningfulAnnotation, Set<String>> recordedAnnotations = annotationsHolder.getRecordedAnnotations();

        // Assert
        assertThat(recordedAnnotations.size(), is(1));
        assertThat(recordedAnnotations, hasEntry(MeaningfulAnnotation.COMPONENT, singleton("com.example.Foo#myName")));
    }

    @Test
    public void getRecordedAnnotations_whenOneInterestingAnnotationRecordedWithNoName_shouldReturnMapWithOneEntry() {
        // Arrange
        final RecordedAnnotationsHolder annotationsHolder = new RecordedAnnotationsHolder();
        annotationsHolder.recordAnnotation(Component.class.getName(), null, "com.example.Foo");

        // Act
        final Map<MeaningfulAnnotation, Set<String>> recordedAnnotations = annotationsHolder.getRecordedAnnotations();

        // Assert
        assertThat(recordedAnnotations.size(), is(1));
        assertThat(recordedAnnotations, hasEntry(MeaningfulAnnotation.COMPONENT, singleton("com.example.Foo")));
    }

    @Test
    public void getRecordedAnnotations_whenOneInterestingAnnotationRecordedTwice_shouldReturnMapWithOneEntry() {
        // Arrange
        final RecordedAnnotationsHolder annotationsHolder = new RecordedAnnotationsHolder();
        annotationsHolder.recordAnnotation(Component.class.getName(), "name1", "com.example.Foo");
        annotationsHolder.recordAnnotation(Component.class.getName(), "name2", "com.example.Foo");

        // Act
        final Map<MeaningfulAnnotation, Set<String>> recordedAnnotations = annotationsHolder.getRecordedAnnotations();

        // Assert
        assertThat(recordedAnnotations.size(), is(1));
        final Set<String> expectedEntries =
                Stream.of("com.example.Foo#name1", "com.example.Foo#name2").collect(toSet());
        assertThat(recordedAnnotations, hasEntry(MeaningfulAnnotation.COMPONENT, expectedEntries));
    }

    @Test
    public void record_whenUninterestingAnnotationRecorded_shouldThrowAnException() {
        // Arrange
        final RecordedAnnotationsHolder annotationsHolder = new RecordedAnnotationsHolder();
        expectedException.expect(IllegalStateException.class);
        expectedException.expectMessage("Annotation javax.annotation.Nullable not found");

        // Act
        annotationsHolder.recordAnnotation(Nullable.class.getName(), "anyName", "anyClass");
    }
}
