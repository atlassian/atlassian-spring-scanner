package com.atlassian.plugin.spring.scanner.annotation.export;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Denotes a component to be exported as an OSGi service available to other bundles.
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExportAsService {
    /**
     * The interfaces the service should be exported as.
     * <p>
     * The default is to export as ALL interfaces implemented by this class,
     * or as the implementation class if it implements no interfaces.
     *
     * @return the list of interfaces to export
     */
    Class<?>[] value() default {};

    /**
     * The service properties to be exported with this service.
     * <p></p>
     * Multiple properties can now be added to a service being exported as shown by <p></p>
     *  {@code @ExportAsService( } <p style="text-indent: 40px">
     *  {@code properties = {@ServiceProperty(key = "my_key", value = "service_with_properties_key")} } <p>
     *  {@code )} <p></p>
     *
     * @since 2.2.0
     * @return The service properties to be exported with the service
     */
    ServiceProperty[] properties() default {};
}
