# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [3.0.2]

- [SCANNER-2180] Upgrade to AMPS 8.6
- [CONFSRVDEV-23900] Upgrade to AMPS 8.6
- [BSP-3850] update commons-io dependency

## [3.0.1]

- [BSP-3784] ComponentImportBeanFactoryPostProcessor fails on Spring 5.3.18
- [BSP-3783] Update org.springframework:spring-beans

## [3.0.0]

- [BSP-3601] Platform 6 version
- [BSP-3437] Guava update
